﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDBehavior : MonoBehaviour {
	
	public Slider healthSlider;
	public Text scoreText;
	public Text currencyText;
	public Image slot0;
	public Image slot1;
	public Image slot2;
	public Image slot3;
	public Image slot4;
	public Image slot5;
	public Sprite activeSlot;
	public Sprite inactiveSlot;
	public GameObject player;

	
	int chosenSlot;
	Image[] inventorySlots = new Image[6];
	
	
	int health;
	int score;
	int currency;
	
	
	// Use this for initialization
	void Start () {
		FillImageArray ();
		healthSlider.value = 100;
        healthSlider.maxValue = 100;
		scoreText.text = "0";
		//slot1.sprite = activeSlot;
	}
	
	// Update is called once per frame
	void Update () {
		//Tager health fra playeren og justerer healthbar
        healthSlider.value = GameManagerScript.instance.health;
        scoreText.text = Config.CurrentScore.ToString();
        currencyText.text = GameManagerScript.instance.Currency.ToString();
        UpdateInventory();
        
	}

	void UpdateInventory()
	{
		//Checker om en af våbenknapperne holdes nede og hvilken
        if (Input.GetKeyDown(KeyCode.Alpha1))
		{
            if (chosenSlot != 0)
            {
                inventorySlots[chosenSlot].sprite = inactiveSlot;
            }
			chosenSlot = 0;
            slot0.sprite = activeSlot;
		}
        if (Input.GetKeyDown(KeyCode.Alpha2))
		{
            if (chosenSlot != 1)
            {
                inventorySlots[chosenSlot].sprite = inactiveSlot;
            }
            chosenSlot = 1;
            slot1.sprite = activeSlot;
		}
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (chosenSlot != 2)
            {
                inventorySlots[chosenSlot].sprite = inactiveSlot;
            }
            chosenSlot = 2;
            slot2.sprite = activeSlot;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (chosenSlot != 3)
            {
                inventorySlots[chosenSlot].sprite = inactiveSlot;
            }
            chosenSlot = 3;
            slot3.sprite = activeSlot;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (chosenSlot != 4)
            {
                inventorySlots[chosenSlot].sprite = inactiveSlot;
            }
            chosenSlot = 4;
            slot4.sprite = activeSlot;
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            if (chosenSlot != 5)
            {
                inventorySlots[chosenSlot].sprite = inactiveSlot;
            }
            chosenSlot = 5;
            slot5.sprite = activeSlot;
        }
        
	}

	void FillImageArray()
	{
		inventorySlots [0] = slot0;
		inventorySlots [1] = slot1;
		inventorySlots [2] = slot2;
		inventorySlots [3] = slot3;
		inventorySlots [4] = slot4;
		inventorySlots [5] = slot5;

	}
	
	//Test af healthbar og score/currency counters
	void TestDrive()
	{
		if (health > 0)
		{
			health -= Random.Range(1, 10);
		}
		else
		{
			health = 100;
		}
		
		score += 10;
		currency++;

	}
}
