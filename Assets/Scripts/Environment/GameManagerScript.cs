﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum WeaponType { Melee, Ranged }

public class GameManagerScript : MonoBehaviour
{
    //Fields
    //static GameManager singleton
    private static GameManagerScript _instance;
    public bool inGame = false;
    public bool playerAlive = true;

    //Weapons
    private HammerScript hammer;
    private MinigunScript minigun;
    private BubblegunScript bubblegun;
    private LauncherScript launcher;

    public GameObject player;
    public List<WeaponBase> ownedWeapons;
    public int currency;
    public float health = 100;
    private float timeSinceKill;

    //Powerups
    public float powerDouble = -1f;
    public bool powerDoubleIsActive = false;
    public float powerFast = -1f;
    public bool powerFastIsActive = false;
    public float powerSplash = -1f;
    public bool powerSplashIsActive = false;
    public float powerHealth = 20f;

    public List<Powerup> ownedPowerups;

    public float powerupDuration = 10f;
    public float speedMultiplier = 1.5f;
    public float splashRadius = 5;
    public int currentDay = 3;

    //Spawner for bunnies
    public List<Carrot> listOfCarrots;
    public List<GameObject> listOfBunnies;

    //Day & Night cycle
    private bool isDay = true;
    public GameObject dayGroupObject;
    public GameObject nightGroupObject;
    private TimeChanger dayGroupScript;
    private TimeChanger nightGroupScript;
    public GameObject skyBoxObject;
    public Texture skyBoxDay;
    public Texture skyBoxNight;
    public GameObject mainLightObject;
    public GameObject fillLightObject;
    private Light mainLight;
    private Light fillLight;

    public float dayTimeCycle = 20f;
    public float nightTimeCycle = 20f;
    private float nextCycleTime = -1f;

    //Score stuff
    public int pointsOnDay = 100;

    //Properties
    public HammerScript Hammer
    {
        get { return hammer; }
        set { hammer = value; }
    }

    public MinigunScript Minigun
    {
        get { return minigun; }
        set { minigun = value; }
    }

    public BubblegunScript Bubblegun
    {
        get { return bubblegun; }
        set { bubblegun = value; }
    }

    public LauncherScript Launcher
    {
        get { return launcher; }
        set { launcher = value; }
    }

    public int Currency
    {
        get { return currency; }
        set { currency = value; }
    }

    public float TimeSinceKill
    {
        get { return timeSinceKill; }
        set { timeSinceKill = value; }
    }

    public bool IsDay
    {
        get { return isDay; }
    }

    //Constructor
    public static GameManagerScript instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManagerScript>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);

            //LoadWeapons();
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }

        // Powerups initialization
        ownedPowerups = new List<Powerup>();

        //LoadWeapons();
        player.GetComponent<AudioSource>().volume = Config.SoundFXVolume;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().volume = Config.SoundFXVolume;

    }

    void Start()
    {
        //dayGroupScript = (TimeChanger)dayGroupObject.GetComponent(typeof(TimeChanger));
        //nightGroupScript = (TimeChanger)nightGroupObject.GetComponent(typeof(TimeChanger));
        //mainLight = mainLightObject.GetComponent<Light>();
        //fillLight = fillLightObject.GetComponent<Light>();
    }

    void Update()
    {
        if (!inGame)
            if (Application.loadedLevel == 1)
                inGame = true;

        if (inGame)
        {
            if (dayGroupScript == null)
                OnChangeToGame();
            // These checks the powerup time and assigns true or false to IsActive to a powerups boolean
            powerDoubleIsActive = (powerDouble > Time.time) ? true : false;
            powerFastIsActive = (powerFast > Time.time) ? true : false;
            powerSplashIsActive = (powerSplash > Time.time) ? true : false;
            timeSinceKill += Time.deltaTime;

            // Time cycle between night and day
            if (nextCycleTime > 0f)
            {
                if (Time.time > nextCycleTime)
                {
                    if (IsDay)
                    {
                        ChangeToNight();
                    }
                    else
                    {
                        ChangeToDay();
                    }
                }
            }

            //if (!powerDoubleIsActive)
            //{
            //    player.GetComponent<Weapon>().currentWeapon.damage = GetComponent<Weapon>().currentWeapon.origDamage;
            //}
            //if (!powerFastIsActive)
            //{
            //    player.GetComponent<PlayerMovement>().maxForwardSpeed = player.GetComponent<PlayerMovement>().origMaxForwardSpeed;
            //    player.GetComponent<PlayerMovement>().maxBackwardSpeed = player.GetComponent<PlayerMovement>().origMaxBackwardSpeed;
            //    player.GetComponent<PlayerMovement>().maxSideSpeed = player.GetComponent<PlayerMovement>().origMaxSideSpeed;
            //}
        }
    }

    //Functions
    public void LoadWeapons()
    {
        //Load Weapons and set fields of singleton to loaded weapon
        Instantiate(Resources.Load("Weapons/Hammer"));//, GameObject.FindGameObjectWithTag("Player").transform.position, Quaternion.identity);// new Hammer();
        hammer = GameObject.FindObjectOfType<HammerScript>();
        hammer.transform.parent = this.transform;
        hammer.GetComponent<AudioSource>().volume = Config.SoundFXVolume;
        Instantiate(Resources.Load("Weapons/Minigun"));//, GameObject.FindGameObjectWithTag("Player").transform.position, Quaternion.identity);// new Hammer();
        minigun = GameObject.FindObjectOfType<MinigunScript>();
        minigun.transform.parent = this.transform;
        minigun.GetComponent<AudioSource>().volume = Config.SoundFXVolume;
        Instantiate(Resources.Load("Weapons/Bubblegun"));
        bubblegun = GameObject.FindObjectOfType<BubblegunScript>();
        bubblegun.transform.parent = this.transform;
        bubblegun.GetComponent<AudioSource>().volume = Config.SoundFXVolume;
        Instantiate(Resources.Load("Weapons/Launcher"));
        launcher = GameObject.FindObjectOfType<LauncherScript>();
        launcher.transform.parent = this.transform;
        launcher.GetComponent<AudioSource>().volume = Config.SoundFXVolume;
        //minigun.GetComponent<AudioSource>().clip = minigun.attackSound[0]

        launcher.renderer.enabled = false;
        HideWeapon(hammer);
        HideWeapon(bubblegun);
        HideWeapon(minigun);

    }

    public void UsePowerupFromList()
    {
        if (ownedPowerups.Count > 0)
        {
            if (ownedPowerups[0] == Powerup.Double) //Double damage
            {
                powerDouble = Time.time + powerupDuration;
                player.GetComponent<Weapon>().currentWeapon.damage = player.GetComponent<Weapon>().currentWeapon.origDamage * 2;
            }
            else if (ownedPowerups[0] == Powerup.Fast) //Increased movement speed
            {
                powerFast = Time.time + powerupDuration;
                player.GetComponent<PlayerMovement>().maxForwardSpeed = player.GetComponent<PlayerMovement>().origMaxForwardSpeed * speedMultiplier;
                player.GetComponent<PlayerMovement>().maxBackwardSpeed = player.GetComponent<PlayerMovement>().origMaxBackwardSpeed * speedMultiplier;
                player.GetComponent<PlayerMovement>().maxSideSpeed = player.GetComponent<PlayerMovement>().origMaxSideSpeed * speedMultiplier;
            }
            else if (ownedPowerups[0] == Powerup.Splash) //Base damage to several enemies
            {
                powerSplash = Time.time + powerupDuration;
            }
            else if (ownedPowerups[0] == Powerup.MedPack)
            {
                health = health + powerHealth;
                if (health > 100)
                    health = 100;
            }
            Debug.Log(ownedPowerups[0]);
            ownedPowerups.RemoveAt(0);
        }
    }

    //Day & Night methods
    public void ChangeToDay()
    {
        isDay = true;
        currentDay += 1;
        Config.CurrentScore += pointsOnDay;
        dayGroupScript.ChangeToDayTime();
        nightGroupScript.ChangeToDayTime();
        nextCycleTime = Time.time + dayTimeCycle;
        GameObject.FindGameObjectWithTag("Shop").GetComponent<ShopScript>().ReloadSelection();
        skyBoxObject.renderer.material.mainTexture = skyBoxDay;
        mainLight.color = Color.Lerp(Color.white, Color.yellow, 0.2f);
        mainLight.intensity = 0.5f;
        fillLight.color = Color.Lerp(Color.white, Color.green, 0.2f);
        fillLight.intensity = 0.1f;
        Config.CurrentScore += 100;
        Debug.Log("Points for day change");
        foreach (GameObject bunny in listOfBunnies)
        {
            if (bunny != null)
            bunny.GetComponent<BunnyAI>().ChangeToDay();
        }
    }

    public void ChangeToNight()
    {
        isDay = false;
        dayGroupScript.ChangeToNightTime();
        nightGroupScript.ChangeToNightTime();
        nextCycleTime = Time.time + nightTimeCycle;

        skyBoxObject.renderer.material.mainTexture = skyBoxNight;
        mainLight.color = Color.Lerp(Color.white, Color.blue, 0.2f);
        mainLight.intensity = 0.4f;
        fillLight.color = Color.cyan;
        fillLight.intensity = 0.04f;
        foreach (GameObject bunny in listOfBunnies)
        {
            if (bunny != null)
            bunny.GetComponent<BunnyAI>().ChangeToNight();
        }
    }

    public void StartGame()
    {
        nextCycleTime = Time.time + dayTimeCycle;
    }

    public void StopGame()
    {
        nextCycleTime = -1f;
    }

    public void DamagePlayer(float damage)
    {
        health = health - damage;
        if (health <= 0)
        {
            playerAlive = false;
            Debug.Log("Player Died!!");
            Config.CheckScore();
        }

    }

    public void OnChangeToGame()
    {
        dayGroupObject = GameObject.FindGameObjectWithTag("Sun");
        nightGroupObject = GameObject.FindGameObjectWithTag("Moon");
        skyBoxObject = GameObject.FindGameObjectWithTag("Skybox");
        mainLightObject = GameObject.FindGameObjectWithTag("MainLight");
        fillLightObject = GameObject.FindGameObjectWithTag("FillLight");

        //Carrot[] tempCarrots = FindObjectsOfType<Carrot>();

        //foreach(Carrot c in tempCarrots)
        //{
        //    listOfCarrots.Add(c);
        //}


        //skyBoxDay = ;
        //skyBoxNight;

        dayGroupScript = (TimeChanger)dayGroupObject.GetComponent(typeof(TimeChanger));
        nightGroupScript = (TimeChanger)nightGroupObject.GetComponent(typeof(TimeChanger));
        mainLight = mainLightObject.GetComponent<Light>();
        fillLight = fillLightObject.GetComponent<Light>();

        LoadWeapons();
        GameObject.FindGameObjectWithTag("Shop").GetComponentInChildren<ShopScript>().ReloadSelection();

    }

    public void HideWeapon(WeaponBase weapon)
    {
        if (weapon.GetComponent<Animator>())
            weapon.GetComponent<Animator>().enabled = false;

        MeshRenderer[] tempList = weapon.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer x in tempList)
        {
            x.renderer.enabled = false;
        }
    }
}
