using UnityEngine;
using System.Collections;

public class Carrot : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        GameManagerScript.instance.listOfCarrots.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManagerScript.instance.IsDay == false && gameObject.collider.enabled == false)
        {
            Respawn();
        }
    }

    void Respawn()
    {
        renderer.enabled = true;
        gameObject.collider.enabled = true;
        GameManagerScript.instance.listOfCarrots.Add(this);
    }

    public void PickUp()
    {
        renderer.enabled = false;
        gameObject.collider.enabled = false;
        GameManagerScript.instance.listOfCarrots.Remove(this);
    }
}
