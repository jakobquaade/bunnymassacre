﻿using UnityEngine;
using System.Collections;

public class ShopItem
{

    public WeaponBase item;
    public string itemName;
    public string description;
    public int itemPrice;
    public int amount;
    public int ammoPrice;

    public ShopItem(WeaponBase item, int price, int ammoPrice, int amountAvailable)
    {
        this.item = item;
        this.itemName = item.itemName;
        this.description = item.description;
        this.itemPrice = price;
        this.ammoPrice = ammoPrice;
        this.amount = amountAvailable;
    }

    public void Draw(Rect mainWindow)
    {
        GUILayout.BeginVertical("box", GUILayout.Height((mainWindow.height / 3) - 20));
        GUILayout.Label(itemName + "  $" + itemPrice);
        GUILayout.Label(description);
        GUILayout.BeginHorizontal("box", GUILayout.Width((mainWindow.width / 3) - 20));

        if (GUILayout.Button("Buy"))
        {
            //Check if price is higher than current currency; if there's more than 0 available, if weapon is already owned
            if (GameManagerScript.instance.Currency >= itemPrice && amount > 0 && !GameManagerScript.instance.ownedWeapons.Contains(item))
            {
                amount--;
                GameManagerScript.instance.Currency = GameManagerScript.instance.Currency - itemPrice;
                GameManagerScript.instance.ownedWeapons.Add(item);
                Debug.Log(itemName + " bought!");
            }
            else
                Debug.Log("Cannot purchase " + itemName);

        }
        if (GUILayout.Button("Restock Ammo"))
        {
            //Check if price is higher than current currency; if there's more than 0 available, if weapon is already owned
            if (GameManagerScript.instance.Currency >= ammoPrice && GameManagerScript.instance.ownedWeapons.Contains(item))
            {
                GameManagerScript.instance.Currency = GameManagerScript.instance.Currency - ammoPrice;
                if (itemName == "Minigun")
                {
                    GameManagerScript.instance.Minigun.ammunition = GameManagerScript.instance.Minigun.maxAmmunition;
                }
                if (itemName == "Launcher")
                {
                    GameManagerScript.instance.Launcher.ammunition = GameManagerScript.instance.Launcher.maxAmmunition;
                }
                if (itemName == "Bubblegun")
                {
                    GameManagerScript.instance.Bubblegun.ammunition = GameManagerScript.instance.Bubblegun.maxAmmunition;
                }
                Debug.Log(itemName + " bought!");
            }
            else
                Debug.Log("Cannot purchase " + itemName + " ammo");

        }

        //Check if player already owns item

        //Show sell button if player owns item
        if (GUILayout.Button("Sell"))
        {
            if (GameManagerScript.instance.ownedWeapons.Contains(item))
            {
                amount++;
                GameManagerScript.instance.Currency = GameManagerScript.instance.Currency + (itemPrice / 2);
                GameManagerScript.instance.ownedWeapons.Remove(item);
                Debug.Log(itemName + " sold!");
            }
            else
                Debug.Log("You don't have any " + itemName + "s to sell!");

            //Remove item from player inventory
            //Add currency to player
        }
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
    }
}
