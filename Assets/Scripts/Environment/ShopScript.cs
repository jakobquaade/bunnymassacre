﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopScript : MonoBehaviour
{


    public WeaponBase[] availableWeapons;
    private Rect mainShopWindow;
    private List<ShopItem> itemsForSale;
    private bool showShop;


    // Use this for initialization
    void Start()
    {
        //Instantiate array and list
        availableWeapons = new WeaponBase[3];
        itemsForSale = new List<ShopItem>();

        //Randomize weapons available
        //for (int i = 0; i < availableWeapons.Length; i++)
        //{
        //    float tempItem = Random.Range(0, 100);
        //    int tempPrice = 0;

        //    if (tempItem < 25)
        //    {
        //        tempPrice = 20;
        //        availableWeapons[i] = GameManagerScript.instance.Minigun;
        //    }
        //    if (tempItem >= 25 && tempItem < 50)
        //    {
        //        tempPrice = 10;
        //        availableWeapons[i] = GameManagerScript.instance.Hammer;
        //    }
        //    if (tempItem >= 50 && tempItem < 75)
        //    {
        //        tempPrice = 100;
        //        availableWeapons[i] = GameManagerScript.instance.Bubblegun;
        //    }
        //    if (tempItem >= 50 && tempItem <= 100)
        //    {
        //        tempPrice = 150;
        //        availableWeapons[i] = GameManagerScript.instance.Launcher;
        //    }

        //    itemsForSale.Add(new ShopItem(availableWeapons[i], tempPrice, 1));
        //}

        mainShopWindow = new Rect(20, 20, Screen.width - 40, Screen.height - 40);

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ReloadSelection()
    {
        itemsForSale.Clear();

        //Randomize weapons available
        //for (int i = 0; i < availableWeapons.Length; i++)
        //{
        //    float tempItem = Random.Range(0, 100);
        //    int tempPrice = 0;
            
        //    if (tempItem < 25)
        //    {
        //        tempPrice = 20;
        //        availableWeapons[i] = GameManagerScript.instance.Minigun;
        //    }
        //    if (tempItem >= 25 && tempItem < 50)
        //    {
        //        tempPrice = 10;
        //        availableWeapons[i] = GameManagerScript.instance.Hammer;
        //    }
        //    if (tempItem >= 50 && tempItem < 75)
        //    {
        //        tempPrice = 100;
        //        availableWeapons[i] = GameManagerScript.instance.Bubblegun;
        //    }
        //    if (tempItem >= 50 && tempItem <= 100)
        //    {
        //        tempPrice = 150;
        //        availableWeapons[i] = GameManagerScript.instance.Launcher;
        //    }

        //Add minigun
        if (GameManagerScript.instance.currentDay >= 2)
            itemsForSale.Add(new ShopItem(GameManagerScript.instance.Minigun, 100, 20, 1));
        if (GameManagerScript.instance.currentDay >= 3)
            itemsForSale.Add(new ShopItem(GameManagerScript.instance.Bubblegun, 100, 20, 1));
        if (GameManagerScript.instance.currentDay >= 4)
            itemsForSale.Add(new ShopItem(GameManagerScript.instance.Launcher, 100, 20, 1));
        //}
    }

    public void OpenShop()
    {
        ReloadSelection();
        print("Shop opened!");
        showShop = true;
        Screen.showCursor = true;

        //Pause game and disable player movement
        //Time.timeScale = 0;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().enabled = false;
        //GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerMovement>().enabled = false;
    }

    public void CloseShop()
    {
        showShop = false;
        Screen.showCursor = false;

        //Resume game and enable player movement
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().enabled = true;
        //GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerMovement>().enabled = true;
        //Time.timeScale = 1;
    }

    private void DrawShopGUI(int windowID)
    {

        //Draw each available item
        GUILayout.BeginHorizontal();
        foreach (ShopItem item in itemsForSale)
        {
            item.Draw(mainShopWindow);
        }
        GUILayout.EndHorizontal();



        //Draw exit button
        GUILayout.BeginArea(new Rect((Screen.width / 3), Screen.height - 100, (Screen.width / 3), 50));

        if (GUILayout.Button("Exit"))
        {
            CloseShop();
        }

        //if (GUILayout.Button("Reload selection of items"))
        //{
        //    ReloadSelection();
        //}

        GUILayout.EndArea();


    }

    private void OnGUI()
    {
        if (showShop)
        {
            mainShopWindow = GUI.Window(0, mainShopWindow, DrawShopGUI, "Shop Window");
        }
    }


}
