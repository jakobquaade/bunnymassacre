﻿using UnityEngine;
using System.Collections;

public class TimeChanger : MonoBehaviour
{

    public float yValueForDay = 5f;
    public float yValueForNight = -5f;
    public float changeSpeed = 32f;
    public bool isDayGroup = true;
    private bool day = true;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isDayGroup == true)
        {
            if (day == true && transform.position.y < yValueForDay)
                transform.Translate(new Vector3(0, changeSpeed, 0) * Time.deltaTime);
            else if (day == false && transform.position.y > yValueForNight)
                transform.Translate(new Vector3(0, -changeSpeed, 0) * Time.deltaTime);
        }
        else
        {
            if (day == true && transform.position.y > yValueForNight)
                transform.Translate(new Vector3(0, -changeSpeed, 0) * Time.deltaTime);
            else if (day == false && transform.position.y < yValueForDay)
                transform.Translate(new Vector3(0, changeSpeed, 0) * Time.deltaTime);
        }
    }

    public void ChangeToDayTime()
    {
        day = true;
    }

    public void ChangeToNightTime()
    {
        day = false;
    }
}
