﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour
{
    public GameObject currency;
    //public GameObject powerup;
    public GameObject doublePowerup;
    public GameObject fastPowerup;
    public GameObject splashPowerup;
    public GameObject healthPowerup;
    private PowerupDrop powerupScript;
    System.Random rnd = new System.Random();



    // Update is called once per frame
    void Update()
    {

    }

    public void DropItem()
    {
        int temp = rnd.Next(1, 5);
        for (int i = 0; i < temp; i++)
        {
            Instantiate(currency, new Vector3(transform.position.x + Random.Range(-1.0f, 1.0f), transform.position.y, transform.position.z + Random.Range(-1.0f, 1.0f)), Quaternion.identity);
        }
        

        if (Random.value < 1f)
        {
            int selectedPowerup = (int)Random.Range(1f, 4.999f);
            Debug.Log(selectedPowerup);
            if (selectedPowerup == 1)
            {
                Instantiate(doublePowerup, new Vector3(transform.position.x + Random.Range(-1.0f, 1.0f), transform.position.y + 1, transform.position.z + Random.Range(-1.0f, 1.0f)), Quaternion.identity);
                powerupScript = doublePowerup.GetComponent<PowerupDrop>();
                powerupScript.powerupType = Powerup.Double;
            }
            else if (selectedPowerup == 2)
            {
                Instantiate(fastPowerup, new Vector3(transform.position.x + Random.Range(-1.0f, 1.0f), transform.position.y + 1, transform.position.z + Random.Range(-1.0f, 1.0f)), Quaternion.identity);
                powerupScript = fastPowerup.GetComponent<PowerupDrop>();
                powerupScript.powerupType = Powerup.Fast;
                
            }
            else if (selectedPowerup == 3)
            {
                Instantiate(splashPowerup, new Vector3(transform.position.x + Random.Range(-1.0f, 1.0f), transform.position.y + 1, transform.position.z + Random.Range(-1.0f, 1.0f)), Quaternion.identity);
                powerupScript = splashPowerup.GetComponent<PowerupDrop>();
                powerupScript.powerupType = Powerup.Splash;
                
            }
            else if (selectedPowerup == 4)
            {
                Instantiate(healthPowerup, new Vector3(transform.position.x + Random.Range(-1.0f, 1.0f), transform.position.y + 1, transform.position.z + Random.Range(-1.0f, 1.0f)), Quaternion.identity);
                powerupScript = healthPowerup.GetComponent<PowerupDrop>();
                powerupScript.powerupType = Powerup.MedPack;
                
            }

            //Instantiate(powerup, new Vector3(transform.position.x + Random.Range(-1.0f, 1.0f), transform.position.y, transform.position.z + Random.Range(-1.0f, 1.0f)), Quaternion.identity);
        }
    }
}
