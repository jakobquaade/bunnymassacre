﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using System.Random;

public class Spawner : MonoBehaviour
{
    public List<GameObject> listSpawnerBunnies;
    public List<GameObject> spawners;

    private int maxSpawn = 1;
    private float spawnMulti;
    bool hasSpawned = true;
    System.Random rnd = new System.Random();
    public int maxCarrots = 12;


    // Use this for initialization
    void Start()
    {
        //rnd = System.Convert.ToInt32(Random.Range(1, 5));
    }

    // Update is called once per frame
    void Update()
    {
        spawnMulti = 1+((maxCarrots - GameManagerScript.instance.listOfCarrots.Count)/20) + (0.10f*GameManagerScript.instance.currentDay);
        Debug.Log(spawnMulti);

        if (GameManagerScript.instance.IsDay == true && hasSpawned == true)
        {
            SpawnOneBySpawners();
            hasSpawned = false;
        }
        else if (GameManagerScript.instance.IsDay == false)
        {
            hasSpawned = true;
        }
    }

    void SpawnOneBySpawners()
    {
        foreach (GameObject item in spawners)
        {

            for (int i = 0; i < maxSpawn * spawnMulti; i++)
            {
                int tempValue = rnd.Next(0, 100);

                GameObject temp = Instantiate(GetSpawnEnemyType(tempValue), new Vector3(item.transform.position.x + Random.Range(-2.0f, 2.0f), item.transform.position.y, item.transform.position.z + Random.Range(-2.0f, 2.0f)), Quaternion.identity) as GameObject;
                //GameObject temp = Instantiate(listSpawnerBunnies[tempValue], new Vector3(item.transform.position.x + Random.Range(-2.0f, 2.0f), item.transform.position.y, item.transform.position.z + Random.Range(-2.0f, 2.0f)), Quaternion.identity) as GameObject;
                temp.GetComponent<NavMeshAgent>().speed = temp.GetComponent<NavMeshAgent>().speed / 2;
                BunnyAI tempScript = temp.GetComponent<BunnyAI>();
                tempScript.target = GameManagerScript.instance.player.transform;
                tempScript.targetCarrot = GameManagerScript.instance.listOfCarrots[Random.Range(0, GameManagerScript.instance.listOfCarrots.Count - 1)].transform;
                tempScript.isDay = GameManagerScript.instance.IsDay;
                GameManagerScript.instance.listOfBunnies.Add(temp);
                if (GameManagerScript.instance.IsDay == true)
                {
                    tempScript.state = AIState.seekingCarrot;
                    //Debug.Log("State sat til Seeking (spawnerIsDay)");
                }
                else
                {
                    tempScript.state = AIState.walking;
                    //Debug.Log("State sat til walking (spawnerIsDay)");
                }
            }
        }
        Debug.Log(GameManagerScript.instance.listOfCarrots.Count + " Carrots");
        Debug.Log(listSpawnerBunnies.Count + " Bunnies");
        Debug.Log(GameManagerScript.instance.listOfBunnies.Count + " List of bunnies");
    }

    GameObject GetSpawnEnemyType(int chance)
    {
        if (chance < 1)
        {
            return listSpawnerBunnies[0]; //Gold
        }
        if (chance >= 1 && chance < 5)
        {
            return listSpawnerBunnies[1]; //Goliath
        }
        if (chance >= 5 && chance < 15)
        {
            return listSpawnerBunnies[2]; //Fast
        }
        if (chance >= 15 && chance < 30)
        {
            return listSpawnerBunnies[3]; //Slow
        }
        if (chance >= 30 && chance < 45)
        {
            return listSpawnerBunnies[4]; //Hopper
        }
        if (chance >= 45 && chance < 60)
        {
            return listSpawnerBunnies[5]; //Leaper
        }
        if (chance >= 60)
        {
            return listSpawnerBunnies[6]; //Normal
        }
        return listSpawnerBunnies[6];
    }
}
