﻿using UnityEngine;
using System.Collections;

public class CurrencyDrop : MonoBehaviour
{
    private float hoverHeight;
    public float timeBeforePickupable = 1f;
    private float activateTime;
    private float createTime;

    // Use this for initialization
    void Start()
    {
        //transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        createTime = Time.time;
        activateTime = createTime + timeBeforePickupable;
        hoverHeight = transform.position.y + 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > activateTime)
        {
            collider.enabled = true;
        }
        //float newScale = Mathf.Lerp(0.1f, 1f, (Time.time - createTime) / timeBeforePickupable);
        //transform.localScale = new Vector3(newScale, newScale, newScale);
        transform.Rotate(new Vector3(0, 30, 0) * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time * 0.2f, 0.2f) + hoverHeight, transform.position.z);
    }
}
