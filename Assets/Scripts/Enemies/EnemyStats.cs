﻿using UnityEngine;
using System.Collections;

public class EnemyStats : MonoBehaviour
{
    //Fields
    public float health = 100;
    public float damage = 10;
    public float speed = 10;
    public int pointsOnDeath = 25;
    public int multiplierOnDeath = 1;

    //Methods
    public void DamageTaken(WeaponBase weapon)
    {
        if (GameManagerScript.instance.powerSplashIsActive)
        {
            ApplySplashDamage(weapon);
        }
        health = health - weapon.damage;
        if (health <= 0)
        {
            Death();
        }

    }

    public void Death()
    {
        if (GameManagerScript.instance.TimeSinceKill < 2f)
        {
            multiplierOnDeath++;
        }
        else
        {
            multiplierOnDeath = 1;
        }

        if (multiplierOnDeath > 1)
        {
            Debug.Log(multiplierOnDeath + "x");
        }

        Destroy(this.gameObject);
        Config.CurrentScore += (pointsOnDeath * multiplierOnDeath);
        Debug.Log("Points for bunny death");

        //Drops a powerup on death, maybe
        Rotator itemDropScript = (Rotator)GetComponent(typeof(Rotator));
        itemDropScript.DropItem();
        GameManagerScript.instance.TimeSinceKill = 0;
    }

    private void ApplySplashDamage(WeaponBase weapon)
    {
        Collider[] splashEnemies = Physics.OverlapSphere(this.gameObject.transform.position, GameManagerScript.instance.splashRadius);

        for (int i = 0; i < splashEnemies.Length; i++)
        {
            splashEnemies[i].GetComponent<EnemyStats>().health = splashEnemies[i].GetComponent<EnemyStats>().health - weapon.damage;

            if (splashEnemies[i].GetComponent<EnemyStats>().health <= 0)
            {
                Death();
            }
        }
    }
}
