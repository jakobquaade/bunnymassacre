﻿using UnityEngine;
using System.Collections;

public enum EnemyState { normal, bubbled, vortexed }

public class EnemyScript : MonoBehaviour
{
    public EnemyState currentState;

    //Bubble-stuff
    #region
    public float timeBubbled;
    public Vector3 bubbledFrom;
    private bool direction;
    public bool bubbleTopped;
    public float timeVortexed;

    public float TimeBubbled
    {
        get { return timeBubbled; }
        set { timeBubbled = value; }
    }
    public Vector3 BubbledFrom
    {
        get { return bubbledFrom; }
        set { bubbledFrom = value; }
    }
    #endregion

    // Use this for initialization
    void Start()
    {
        currentState = EnemyState.normal;
        bubbleTopped = false;
        direction = true;
    }

    // Update is called once per frame
    void Update()
    {

        //Bubble-stuff
        if (currentState == EnemyState.bubbled && Time.time - TimeBubbled <= GameManagerScript.instance.Bubblegun.bubbleLength)
        {
            if (this.rigidbody && !this.rigidbody.isKinematic)
                this.rigidbody.velocity = new Vector3(0, 0, 0);
            if (direction)
            {
                this.transform.Translate(Vector3.up * Time.deltaTime * GameManagerScript.instance.Bubblegun.bubbleLiftSpeed, Space.World);

                if (bubbleTopped)
                {
                    if (this.transform.position.y - BubbledFrom.y >= GameManagerScript.instance.Bubblegun.bubbleHeight + GameManagerScript.instance.Bubblegun.bubblingDistance)
                    {
                        direction = false;
                    }
                }
                
                if (!bubbleTopped && this.transform.position.y - bubbledFrom.y >= GameManagerScript.instance.Bubblegun.bubbleHeight + GameManagerScript.instance.Bubblegun.bubblingDistance)
                {
                    bubbleTopped = true;
                    direction = false;
                }
            }
            else
            {
                this.transform.Translate(Vector3.down * Time.deltaTime * GameManagerScript.instance.Bubblegun.bubbleLiftSpeed, Space.World);
                
                if (bubbleTopped)
                    if (this.transform.position.y - BubbledFrom.y <= GameManagerScript.instance.Bubblegun.bubbleHeight - GameManagerScript.instance.Bubblegun.bubblingDistance)
                    {
                        direction = true;
                    }

                if (this.transform.position.y - BubbledFrom.y <= GameManagerScript.instance.Bubblegun.bubbleHeight - GameManagerScript.instance.Bubblegun.bubblingDistance)
                    {
                        direction = true;
                    }
            }
        }

        else
        {
            if (currentState != EnemyState.vortexed)
            {
                currentState = EnemyState.normal;
                bubbleTopped = false;
            }
            else
            {
                if (Time.time - timeVortexed > 1)
                {
                    currentState = EnemyState.normal;
                }
            }
        }

    }

    public void IsHit(WeaponBase weapon)
    {
        if (weapon != GameManagerScript.instance.Bubblegun)
        {
            currentState = EnemyState.normal;
        }

        if (weapon != GameManagerScript.instance.Launcher)
        {
            this.gameObject.GetComponent<BunnyAI>().AddKnockback((this.transform.position - weapon.transform.position).normalized * weapon.knockback);
            GetComponent<AIStats>().Damage(weapon);
        }
        
        //this.rigidbody.AddForce((this.transform.position - weapon.transform.position).normalized * weapon.knockback, ForceMode.Impulse);
        //Debug.Log("IsHit called correctly");
        //Destroy(this.gameObject);
    }
}
