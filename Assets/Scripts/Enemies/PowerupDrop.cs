﻿using UnityEngine;
using System.Collections;
//using System.Linq;

public enum Powerup { Double, Fast, Splash , MedPack};

public class PowerupDrop : MonoBehaviour
{
    private float hoverHeight;
    public float timeBeforePickupable = 1f;
    private float activateTime;
    private float createTime;
    public Powerup powerupType;

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        createTime = Time.time;
        activateTime = createTime + timeBeforePickupable;
        hoverHeight = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > activateTime)
        {
            GetComponentInChildren<SphereCollider>().enabled = true;
        }
        float newScale = Mathf.Lerp(0.1f, 1f, (Time.time - createTime) / timeBeforePickupable);
        transform.localScale = new Vector3(newScale, newScale, newScale);
        transform.Rotate(new Vector3(0, -45, 0) * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time * 0.2f, 0.2f) + hoverHeight, transform.position.z);
    }

    public void UsePowerup()
    {
        if (powerupType == Powerup.Double)
            GameManagerScript.instance.powerDouble = Time.time + GameManagerScript.instance.powerupDuration;
        else if (powerupType == Powerup.Fast)
            GameManagerScript.instance.powerFast = Time.time + GameManagerScript.instance.powerupDuration;
        else if (powerupType == Powerup.Splash)
            GameManagerScript.instance.powerSplash = Time.time + GameManagerScript.instance.powerupDuration;
        else if (powerupType == Powerup.MedPack)
        {
            GameManagerScript.instance.health += GameManagerScript.instance.powerHealth;
            if (GameManagerScript.instance.health > 100)
                GameManagerScript.instance.health = 100;
        }
            

        Destroy(gameObject);
    }

    public void AddPowerupToList()
    {
        GameManagerScript.instance.ownedPowerups.Add(powerupType);
        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        UsePowerup();
        Destroy(this.gameObject);
    }
}
