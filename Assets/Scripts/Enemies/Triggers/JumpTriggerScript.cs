﻿using UnityEngine;
using System.Collections;

public class JumpTriggerScript : MonoBehaviour {

    //This is a helper class that can be added on a trigger. BunnyAI will then use it.

    public bool useAttack; //If true, the bunny will enter its attack state and do damage, rather than just knockback.
    public bool useTarget; //If true, the jump/attack is done towards the target transform, rather than the vector3 direction.
    public Vector3 direction; //If usetarget is false, it uses this direction;
    public Transform target; //if usetarget is true, we jump towards this transform (could be an empty gameobject).

    public float force; //How fast to jump towards. This is only used on targets.
}
