﻿using UnityEngine;
using System.Collections;

public enum AIState { idle, walking, preAttack, midAttack, knockbacked, getUp, dead, seekingCarrot, eatingCarrot, flee }

public class BunnyAI : MonoBehaviour
{

    //Pathfinding
    public bool isKnockbacked; //If true, we're currently affected by a knockback

    public Animator animator;
    private NavMeshAgent agent;
    public Transform target;

    private float targetTime;
    public float rePathInterval; //How often we should do a .SetDestinaion.

    //General stats
    public AIState state;
    public bool isDay; //If true, the bunny will look for carrots instead, eat them when near, and flee if the player comes too close.

    //Attacking
    public float attackBuildUpTime; // This is the time the bunny is in its "pre-attack" pose. After that, it will jump
    public float attackRange; //How far it will jump from
    public float attackForce; //How much force it will jump with.
    private float attackTimer;

    private float preAttackRotationTime = 10; //Smoothing on the rotation during preAttack.
    public float attackHeightAdd = 0; //Adds this ammount of force upwards on every attack.

    //Daytime stats
    public float fleeDistance; //Bunnies will flee when within this distance of the target.
    public float stopFleeDistance; //Bunnies will revert to looking for carrots when they get this far from the player.
    public Transform targetCarrot; //The carrot they are going for.
    public float eatTime; //How long the bunny has to be eating a carrot for it to dissapear.
    private float eatDistance = 1;
    private float targetEatTime;

    //Knockback
    private Rigidbody rigidbody;
    private BoxCollider boxCol;
    public float wakeUpMagnitude; //Then the magnitude is below this, the actor wakes up from knockback

    public float getUpTime; //How long (in seconds) it takes for a bunny to re-orient itself after being knocked down.
    private float getUpCurrentTime; //The timer for waking up.
    private Vector3 getUpPosition;
    private float getUpRayOffset = 0.2f;

    //Apparently, Unity has some weird issues with Destroy(object), so we make sure we don't Knockback them right after one has ended. Don't ask why, just accept it
    //And move on.
    private bool canBeKnockbacked = true;
    private float knockbackCooldown = 0.2f;
    private float cooldownEndsAt;

    public AIState tempState;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        boxCol = GetComponent<BoxCollider>();
        rigidbody = GetComponent<Rigidbody>();
        //agent.speed = speed;
    }

    void Start()
    {
        this.animator = GetComponent<Animator>();
        RePath();
        if (animator.GetInteger("CurrentState") != 1)
            animator.SetInteger("CurrentState", 1);
    }

    //Check if we are moving slow enough to get up, whenever we touch something. This is to make sure we don't leave the knockbacked state midair.
    void OnCollisionStay()
    {
        //If we are knockbacked
        if (isKnockbacked)
        {
            if (rigidbody.velocity.magnitude < wakeUpMagnitude)
            {
                GetUp();
            }
        }
    }

    //This is for finding out when we hit the player
    void OnCollisionEnter(Collision collision)
    {
        if (state == AIState.midAttack)
        {
            if (collision.gameObject.tag == "Player")
            {
                Debug.Log("Player took damage");
                animator.SetInteger("CurrentState", 2);
                state = AIState.knockbacked;
                GameManagerScript.instance.DamagePlayer(GetComponent<AIStats>().damage);
                GameManagerScript.instance.player.GetComponentInChildren<CameraShake>().AddShake(0.2f, true, 1);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "JumpTrigger")
        {
            JumpTrigger(other.gameObject);
        }
    }

    void JumpTrigger(GameObject trigger)
    {
        JumpTriggerScript script = trigger.GetComponent<JumpTriggerScript>();
        //Jump using the target
        if (script.useTarget)
        {
            AddKnockback((script.target.position - transform.position).normalized * script.force);
            //If it's an attack, set the state accordingly
            if (script.useAttack)
            {
                animator.SetInteger("CurrentState", 2);
                state = AIState.midAttack;
            }
        }
        else
        {
            AddKnockback(script.direction);
            //If it's an attack, set the state accordingly
            if (script.useAttack)
            {
                animator.SetInteger("CurrentState", 2);
                state = AIState.midAttack;
            }
        }
    }

    public void ChangeToNight()
    {
        state = AIState.walking;
        isDay = false;
        //changeStateToWalkingNextUpdate = 10;
    }

    public void ChangeToDay()
    {
        state = AIState.seekingCarrot;
        isDay = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Im so sorry.
        isDay = GameManagerScript.instance.IsDay;
        if (!isDay && state == AIState.idle) { state = AIState.walking; }

        if (targetCarrot == null)
        {
            targetCarrot = GameManagerScript.instance.listOfCarrots[Random.Range(0, GameManagerScript.instance.listOfCarrots.Count - 1)].transform;
        }

        switch (state)
        {
            case AIState.idle:
                UpdateIdle();
                break;

            case AIState.walking:
                UpdateWalking();
                break;

            case AIState.preAttack:
                UpdatePreAttack();
                break;

            case AIState.midAttack:
                UpdateMidAttack();
                break;

            case AIState.knockbacked:
                UpdateKnockBacked();
                break;

            case AIState.getUp:
                UpdateGetUp();
                break;

            case AIState.dead:
                UpdateDead();
                break;

            case AIState.seekingCarrot:
                UpdateSeekingCarrot();
                break;

            case AIState.eatingCarrot:
                UpdateEatingCarrot();
                break;

            case AIState.flee:
                UpdateFlee();
                break;
        }

        
        //Reset the knockbackCooldown if the timer done
        if (cooldownEndsAt > Time.time)
        {
            canBeKnockbacked = true;
        }
    }

    void UpdateIdle()
    {
        agent.Stop();
    }

    void UpdateWalking()
    {
        RePath(); //Check if we should repath, and if so, do that.
        //Check if we are close enough to the player to attack it.
        if (Vector3.Distance(transform.position, target.position) < attackRange)
        {
            animator.SetInteger("CurrentState", 0);
            state = AIState.preAttack;
            attackTimer = Time.time + attackBuildUpTime;
        }
    }

    void UpdatePreAttack()
    {
        agent.Stop(true);
        //Look at the player, but only with the Y axis
        transform.LookAt(target.position);

        if (Time.time > attackTimer)
        {
            Attack();
        }
    }

    void UpdateMidAttack()
    {

    }

    void UpdateKnockBacked()
    {

    }

    void UpdateGetUp()
    {
        Quaternion targetRot = Quaternion.Euler(new Vector3(0, transform.localRotation.y, 0));

        transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRot, getUpTime * Time.deltaTime);
        transform.localPosition = Vector3.Lerp(transform.localPosition, getUpPosition, getUpTime * Time.deltaTime);
        getUpCurrentTime += Time.deltaTime;

        if (getUpCurrentTime >= getUpTime)
        {
            if (animator.GetInteger("CurrentState") != 1)
                animator.SetInteger("CurrentState", 1);
            if (!isDay)
            {
                state = AIState.walking;
            }
            else
            {
                state = AIState.seekingCarrot;
            }
        }
    }

    void UpdateDead()
    {
        animator.SetInteger("CurrentState", 3);
    }

    void UpdateSeekingCarrot()
    {
        if (Vector3.Distance(target.position, transform.position) < fleeDistance)
        {
            if (animator.GetInteger("CurrentState") != 1)
            {
                animator.SetInteger("CurrentState", 1);
            }
            state = AIState.flee;
        }
        else
        {
            RePath();
        }

        if (Vector3.Distance(targetCarrot.position, transform.position) < eatDistance)
        {
            animator.SetInteger("CurrentState", 0);
            targetEatTime = Time.time + eatTime;
            state = AIState.eatingCarrot;
        }
    }

    void UpdateEatingCarrot()
    {
        //Debug.Log("Køre update eating carrot");
        // && GameManagerScript.instance.listOfCarrots.Count >= 0
        if (Time.time > targetEatTime)
        {
            targetCarrot.GetComponent<Carrot>().PickUp();
            state = AIState.idle;
        }
        /*else if (GameManagerScript.instance.listOfCarrots.Count <= 0)
        {
            state = AIState.idle;
            Debug.Log("State sat til idle (UpdateEatingCarrot Else)");
        }
        */
        if (Vector3.Distance(target.position, transform.position) < fleeDistance)
        {
            state = AIState.flee;
        }
    }

    void UpdateFlee()
    {
        Vector3 fleePos = transform.position;
        fleePos = Vector3.MoveTowards(fleePos, target.position, fleeDistance * -1);
        agent.SetDestination(fleePos);

        if (Vector3.Distance(target.position, transform.position) > stopFleeDistance)
        {
            if (animator.GetInteger("CurrentState") != 1)
                animator.SetInteger("CurrentState", 1);
            state = AIState.seekingCarrot;
        }
    }

    void RePath()
    {
        if (state == AIState.walking)
        {
            //RePath whenever we reach the target time
            if (Time.time > targetTime)
            {
                agent.SetDestination(target.position);
                targetTime = Time.time + rePathInterval;
            }
        }
        if (state == AIState.seekingCarrot)
        {
            if (Time.time > targetTime)
            {
                agent.SetDestination(targetCarrot.position);
                targetTime = Time.time + rePathInterval;
            }
        }
    }

    void Attack()
    {
        AddKnockback((target.position - transform.position).normalized * attackForce);
        AddKnockback(new Vector3(0, attackHeightAdd, 0));
        animator.SetInteger("CurrentState", 2);
        state = AIState.midAttack; //Not the cleanest way to launch the player, but it gets the job done. We have to change the state after the knockback
        //Since AddKnockback changes the state to Knockbacked.
    }

    public void AddKnockback(Vector3 knockback)
    {
        if (canBeKnockbacked)
        {
            //Debug.Log("Added knockback");
            //Just add more force if we already are knockbacked.
            if (isKnockbacked)
            {
                rigidbody.AddForce(knockback);
            }

            //If we're currently working as a NavMeshAgent, we need to become a physics object first.
            if (!isKnockbacked)
            {
                agent.enabled = false;

                rigidbody.isKinematic = false;

                //Add the knockback
                rigidbody.AddForce(knockback);
                isKnockbacked = true;
                state = AIState.knockbacked;
                animator.SetInteger("CurrentState", 2);
            }
        }
    }

    void GetUp() //Reverts the agent to its pathfinding state
    {
        rigidbody.isKinematic = true;

        agent.enabled = true;
        isKnockbacked = false;

        canBeKnockbacked = false;
        cooldownEndsAt = Time.time + knockbackCooldown;
        animator.SetInteger("CurrentState", 0);
        state = AIState.getUp;
        getUpCurrentTime = 0;

        getUpPosition = transform.position; //This is a fallback, so that if theres no hit ray, he won't fly into the sunset (or to the last GetUpPos)

        RaycastHit hit;
        Vector3 raycastPos = new Vector3(transform.position.x, transform.position.y - getUpRayOffset, transform.position.z);
        if (Physics.Raycast(raycastPos, -Vector3.up, out hit))
        {
            getUpPosition = hit.point;
        }
    }
}
