﻿using UnityEngine;
using System.Collections;

public class AIStats : MonoBehaviour {

    public float health;
    public float damage;
    public float speed;
    public int pointsOnDeath;
    public int multiplierOnDeath;

    public BunnyAI aiScript;

    public GameObject explodeDeathEffect; //The death object when exploding into gibs

    //public void Damage(float dmg)
    //{
    //    health -= dmg;
    //    if(health <= 0)
    //    {
    //        Kill();
    //    }
    //}
    public void Damage(WeaponBase weapon)
    {
        health = health - weapon.damage;
        if (health <= 0)
        {
            Kill();
        }
    }

    public void Kill()
    {
        //Create the visual effect for when the object dies, and then remove it.
        Instantiate(explodeDeathEffect, transform.position, Quaternion.identity);
        if (GameManagerScript.instance.TimeSinceKill < 2f)
        {
            multiplierOnDeath++;
        }
        else
        {
            multiplierOnDeath = 1;
        }

        Config.CurrentScore += (pointsOnDeath * multiplierOnDeath);

        Rotator itemDropScript = (Rotator)GetComponent(typeof(Rotator));
        itemDropScript.DropItem();
        GameManagerScript.instance.TimeSinceKill = 0;

        Destroy(gameObject);
        
        
    }

    public void Knockback(Vector3 knockback)
    {
        aiScript.AddKnockback(knockback);
    }
}
