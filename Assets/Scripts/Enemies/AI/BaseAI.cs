﻿using UnityEngine;
using System.Collections;

public class BaseAI : MonoBehaviour
{

    //Pathfinding
    public bool isKnockbacked; //If true, we're currently affected by a knockback

    private NavMeshAgent agent;
    public Transform target;

    private float targetTime;
    public float rePathInterval; //How often we should do a .SetDestinaion.

    //Knockback
    private Rigidbody rigidbody;
    private SphereCollider sphereCol;
    public float wakeUpMagnitude; //Then the magnitude is below this, the actor wakes up from knockback

    public float rbAngularDrag; //Angular drag on the rigidbody
    public float rbMass; //The mass of the rigidbody
    public float rbDrag; //The drag.

    //AI
    public float jumpRange;
    public float jumpSpeed;

    //Apparently, Unity has some weird issues with Destroy(object), so we make sure we don't Knockback them right after one has ended. Don't ask why, just accept it
    //And move on.
    private bool canBeKnockbacked = true;
    private float knockbackCooldown = 0.2f;
    private float cooldownEndsAt;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        sphereCol = GetComponent<SphereCollider>();

        sphereCol.enabled = false;
    }

    void Start()
    {
        RePath();
    }

    void OnCollisionStay()
    {
        //If we are knockbacked
        if (isKnockbacked)
        {

            if (rigidbody.velocity.magnitude < wakeUpMagnitude)
            {
                StopKnockback();

            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        ////If we are knockbacked
        //if (isKnockbacked)
        //{

        //    if (rigidbody.velocity.magnitude < wakeUpMagnitude)
        //    {
        //        StopKnockback();

        //    }
        //}


        //If we are not currently knockbacked
        if (!isKnockbacked)
        {
            //RePath whenever we reach the target time
            if (Time.time > targetTime)
            {
                RePath();
            }

            if (Vector3.Distance(transform.position, target.position) < jumpRange)
            {
                AddKnockback((target.position - transform.position).normalized * jumpSpeed);
            }
        }


        if (Input.GetButtonDown("Fire1"))
        {
            AddKnockback(new Vector3(0, 1500, 0));
        }

        if (cooldownEndsAt > Time.time)
        {
            canBeKnockbacked = true;
        }

    }

    void RePath()
    {
        agent.SetDestination(target.position);
        targetTime = Time.time + rePathInterval;
    }

    void AddKnockback(Vector3 knockback)
    {
        if (canBeKnockbacked)
        {
            //Debug.Log("Added knockback");
            //Just add more force if we already are knockbacked.
            if (isKnockbacked)
            {
                rigidbody.AddForce(knockback);
            }

            //If we're currently working as a NavMeshAgent, we need to become a physics object first.
            if (!isKnockbacked)
            {
                agent.enabled = false;
                sphereCol.enabled = true;

                //We should never have a rigidbody when we are comming from !isKnockbacked, but you never know...
                if (rigidbody == null)
                {
                    rigidbody = gameObject.AddComponent<Rigidbody>();

                }

                //Update the rigidbody variables
                rigidbody.angularDrag = rbAngularDrag;
                rigidbody.drag = rbDrag;
                rigidbody.mass = rbMass;

                //Add the knockback
                rigidbody.AddForce(knockback);
                isKnockbacked = true;
            }
        }
    }

    void StopKnockback() //Reverts the agent to its pathfinding state
    {
        sphereCol.enabled = false;
        Destroy(rigidbody);

        agent.enabled = true;
        isKnockbacked = false;

        canBeKnockbacked = false;
        cooldownEndsAt = Time.time + knockbackCooldown;
    }
}
