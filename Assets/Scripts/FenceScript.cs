﻿using UnityEngine;
using System.Collections;

public class FenceScript : MonoBehaviour
{

    RaycastHit hit;
    Ray ray;
    Vector3 position;
    int length = 2;
    GameObject bunny;

    void Start()
    {
        AssignRay();
    }

    void Update()
    {
        Debug.DrawRay(position, Vector3.right*length);
        if (Physics.Raycast(ray, out hit, length))
        {
            if (hit.collider.tag == "Enemy")
	        {
                //Kaninen skal hoppe over hegnet, når den rammer strålen
                //bunny = hit.collider.gameObject.GetComponent<ScriptWithBunnyJumping>();
                //bunny.Jump;
            }
        }

    }


    void AssignRay()
    {
        position = this.gameObject.transform.position;
        //i if-sætningerne skal der checkes på specifik position, fx:
        //højre hegn: Skriv if (position.x == ?) hvor ? er x-koordinaten på hegnet.
        //Ligeledes for z
        if (position.x > 0)
        {
            ray = new Ray(position, Vector3.right);
        }
        else if (position.x < 0)
        {
            ray = new Ray(position, Vector3.left);
        }
        else if (position.z > 0)
        {
            ray = new Ray(position, Vector3.forward);
        }
        else if (position.z < 0)
        {
            ray = new Ray(position, Vector3.back);
        }

    }
}






