﻿using UnityEngine;
using System.Collections;

public class Vortex : MonoBehaviour
{
    public float gravityStrength;
    public float vortexLength;
    public float timeSpawned;
    public Collider[] queuedTargets;
    private bool direction;
    private Vector3 vortexStartPosition;

    // Use this for initialization
    void Start()
    {
        vortexStartPosition = this.transform.position;
        timeSpawned = Time.time;
        direction = true;

        queuedTargets = Physics.OverlapSphere(this.GetComponent<SphereCollider>().transform.position, this.GetComponent<SphereCollider>().radius);

        foreach (Collider col in queuedTargets)
        {
            if (col.GetComponent<BunnyAI>())
            {
                if (col.GetComponent<EnemyScript>().currentState == EnemyState.bubbled)
                {
                    col.GetComponent<EnemyScript>().currentState = EnemyState.normal;
                }
                col.GetComponent<BunnyAI>().AddKnockback((this.transform.position - col.transform.position).normalized * gravityStrength);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Time.time - timeSpawned > vortexLength)
        {
            Destroy(this.gameObject);
        }

        if (direction)
        {
            this.transform.Translate(Vector3.up * Time.deltaTime * GameManagerScript.instance.Bubblegun.vortexLiftSpeed, Space.World);
            if (this.transform.position.y - vortexStartPosition.y >= GameManagerScript.instance.Bubblegun.vortexHeight + GameManagerScript.instance.Bubblegun.vortexDistance)
            {
                direction = false;
            }
        }

        else
        {
            this.transform.Translate(Vector3.down * Time.deltaTime * GameManagerScript.instance.Bubblegun.vortexLiftSpeed, Space.World);
            if (this.transform.position.y - vortexStartPosition.y <= GameManagerScript.instance.Bubblegun.vortexHeight - GameManagerScript.instance.Bubblegun.vortexDistance)
            {
                direction = true;
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<EnemyScript>())
        {
            other.GetComponent<BunnyAI>().AddKnockback((this.transform.position - other.transform.position).normalized * gravityStrength);

            if (other.GetComponent<EnemyScript>().currentState != EnemyState.vortexed)
            {
                other.GetComponent<EnemyScript>().timeVortexed = Time.time;
                other.GetComponent<EnemyScript>().currentState = EnemyState.vortexed;
                
            }

            if (other.GetComponent<BunnyAI>().state != AIState.knockbacked)
            {
                other.GetComponent<BunnyAI>().state = AIState.knockbacked;
                other.GetComponent<BunnyAI>().isKnockbacked = true;
            }

            if (this.transform.position - other.transform.position == Vector3.zero)
            {
                other.GetComponent<BunnyAI>().AddKnockback(Vector3.up * gravityStrength);
            }
            else
            {
                
                other.GetComponent<BunnyAI>().AddKnockback((this.transform.position - other.transform.position).normalized * gravityStrength);

            }
        }
    }
}
