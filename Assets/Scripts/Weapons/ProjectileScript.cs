﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour
{
    public WeaponBase weaponOrigin;
    public int attackType;
    public GameObject projectileExplosion;
    public GameObject mine;
    public GameObject vortex;
    public GameObject bubble;
    public Vector3 endPosition;

    private float spawnTime;
    public float existForXSeconds;



    // Use this for initialization
    void Start()
    {
        spawnTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - spawnTime > existForXSeconds)
            Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        
        //if (other.GetComponent<EnemyScript>())
        //{
            
        //    //Destroy(this.gameObject);
        //}
        
        if (!other.GetComponent<PlayerMovement>() && other.gameObject.name != "Crosshair" && !other.gameObject.GetComponentInParent<Camera>() && !other.gameObject.GetComponent<Vortex>())
        {
            ProjectileProperty(other);
        }
            
            
    }

    void ProjectileProperty(Collider other)
    {
        switch(weaponOrigin.itemName)
        {
            
            case "Hammer":
                {
                    break;
                }
            case "Minigun":
                {
                    if (attackType == 1)
                    {
                        if (other.GetComponent<EnemyScript>())
                        {
                            other.GetComponent<EnemyScript>().IsHit(weaponOrigin);
                            Destroy(this.gameObject);
                        }
                    }
                    break;
                }
            case "Bubblegun":
                {
                    if (attackType == 1)
                    {
                        if (other.GetComponent<EnemyScript>())
                        {
                            other.gameObject.GetComponent<EnemyScript>().IsHit(weaponOrigin);

                            if (other.GetComponent<EnemyScript>().currentState == EnemyState.normal)
                            {
                                other.GetComponent<EnemyScript>().currentState = EnemyState.bubbled;
                                other.GetComponent<EnemyScript>().TimeBubbled = Time.time;
                                other.GetComponent<EnemyScript>().BubbledFrom = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
                                GameObject tempBubble = Instantiate(bubble, other.transform.position, Quaternion.identity) as GameObject;
                                tempBubble.GetComponent<Bubble>().target = other.gameObject;
                                Destroy(this.gameObject);
                                break;
                            }
                            Destroy(this.gameObject);
                            break;
                        }
                        else
                            Destroy(this.gameObject);
                        break;
                    }
                    if (attackType == 2)
                    {
                        Instantiate(vortex, this.transform.position, Quaternion.identity);
                        Destroy(this.gameObject);
                    }
                    break;
                }
            case "Launcher":
                {
                    if (attackType == 1)
                    {
                        if (other.GetComponent<EnemyScript>())
                        {
                            Debug.Log("Explosion should happen");
                            GameObject tempExplosion = Instantiate(projectileExplosion, other.transform.position, Quaternion.identity) as GameObject;
                            tempExplosion.GetComponent<Explosion>().weaponOrigin = weaponOrigin;
                            tempExplosion.GetComponent<Explosion>().explosionPower = weaponOrigin.damage;
                            //explosion.GetComponent<Rigidbody>().AddExplosionForce(weaponOrigin.GetComponent<LauncherScript>().explosionPower, other.transform.position, weaponOrigin.GetComponent<LauncherScript>().explosionSize);
                            //explosion.GetComponent<SphereCollider>().radius = weaponOrigin.GetComponent<LauncherScript>().explosionSize;
                            //explosion.GetComponent<Explosion>().explosionPower = weaponOrigin.GetComponent<LauncherScript>().explosionPower;
                            //Destroy(explosion.gameObject);
                            Destroy(this.gameObject);
                            break;
                        }

                        else
                        {
                            Debug.Log("Ground Explosion should happen");
                            GameObject tempExplosion = Instantiate(projectileExplosion, this.transform.position, Quaternion.identity) as GameObject;
                            tempExplosion.GetComponent<Explosion>().weaponOrigin = weaponOrigin;
                            tempExplosion.GetComponent<Explosion>().explosionPower = weaponOrigin.damage;
                            //explosion.GetComponent<SphereCollider>().radius = weaponOrigin.GetComponent<LauncherScript>().explosionSize;
                            //explosion.GetComponent<Explosion>().explosionPower = weaponOrigin.GetComponent<LauncherScript>().explosionPower;
                            //explosion.GetComponent<Rigidbody>().AddExplosionForce(weaponOrigin.GetComponent<LauncherScript>().explosionPower, other.transform.position, weaponOrigin.GetComponent<LauncherScript>().explosionSize);
                            //Destroy(explosion.gameObject);
                            Destroy(this.gameObject);
                            break;
                        }
                    }
                    if (attackType == 2)
                    {
                        
                        if (other.GetComponent<EnemyScript>())
                        {
                            Instantiate(projectileExplosion, other.transform.position, Quaternion.identity);
                            Destroy(this.gameObject);
                        }
                        else
                        {
                            Debug.Log("Mine should be placed!");
                            Instantiate(mine, this.transform.position, this.transform.rotation);
                            Destroy(this.gameObject);
                        }
                        break;
                    }
                    break;
                }

            default:
                break;
        }
    }
}
