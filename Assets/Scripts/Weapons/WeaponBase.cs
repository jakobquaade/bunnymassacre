﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class WeaponBase : MonoBehaviour {

    //Fields
    public string itemName;
    public string description;
    public Vector3 modelPosOffset; // Holds position relative to parent 
    public Quaternion modelRotOffset; // Holds rotation relative to parent
    public GameObject crosshair;

    //First attack
    public WeaponType weaponType; // Enum weapon type, enum is in GameManager singleton
    public float coolDown; // Weapon Cooldown
    public float range; // Weapon Range
    public float attackSpeed; // Weapon Attackspeed, time it takes to finish attack animation
    public float damage;  // Weapon Damage - can be changed by powerups
    public float origDamage; //Weapon Damage - cannot be changed
    public float lastAttack; // Last time an attack was performed
    public float knockback;
    public GameObject projectile;
    public int ammunition; // Weapon ammunition
    public int maxAmmunition;
    //public AudioClip attackSound;
    public AudioClip[] attackSounds;

    //Secondary attack
    public WeaponType weaponType2; // Enum weapon type, enum is in GameManager singleton
    public float coolDown2; // Weapon Cooldown
    public float range2; // Weapon Range
    public float attackSpeed2; // Weapon Attackspeed, time it takes to finish attack animation
    public float damage2;  // Weapon Damage - can be changed by powerups
    public float origDamage2; //Weapon Damage - cannot be changed
    public float lastAttack2; // Last time an attack was performed
    public float knockback2;
    public int maxAmmunition2;
    public GameObject projectile2;
    public int ammunition2; // Weapon ammunition
    public AudioClip attackSound2;

	// Use this for initialization
	void Start () {

        //Sets this object as a child of the main camera
        this.transform.parent = GameObject.FindGameObjectWithTag("MainCamera").transform;
    
	}
	
	// Update is called once per frame
	public abstract void Update();

    public abstract void Attack();

    public abstract void Attack2();
}
