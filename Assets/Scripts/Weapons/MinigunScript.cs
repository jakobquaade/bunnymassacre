﻿using UnityEngine;
using System.Collections;

public class MinigunScript : WeaponBase {

    //private Vector3 barrelPosition;
    private string minigunPath = "Sound/Effects/Weapons/Minigun/Minigun_";
    public float windUpDuration;
    public float currentWindUp;
    public bool windDirection;
    public GameObject bulletOrigin;

	//Initializing minigun
	void Start () {

        crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        itemName = "Minigun";
        description = "A trusty minigun. Almost guarantees a kill-streak. Super effective against anything remotely cute or cuddly.";

        modelPosOffset = new Vector3(1.043299f, -0.5800395f, 1.7004f);
        modelRotOffset = Quaternion.Euler(90f, 5.029445f, 0f);
        //barrelPosition = new Vector3(4.768371e-09f, -1.907349e-08f, 0.2439829f);
        //this.GetComponent<AudioSource>().clip = attackSound[0];

        //Fields 
        windUpDuration = 0.5f;
        currentWindUp = 0f;

        //First attack
        weaponType = WeaponType.Ranged;
        coolDown = 0.075f;
        range = 10f;
        attackSpeed = 60f;
        damage = 25f;
        lastAttack = Time.time;
        maxAmmunition = 1000;
        ammunition = 1000;
        knockback = 100;

        //Secondary attack
        weaponType2 = WeaponType.Ranged;
        coolDown2 = 0.05f;
        range2 = 10f;
        attackSpeed2 = 1f;
        damage2 = 100f;
        lastAttack2 = Time.time;
        ammunition2 = 100;
        knockback2 = 30;

        //Sounds
        attackSounds = new AudioClip[]{
			Resources.Load(minigunPath + "WindUp") as AudioClip,
			Resources.Load(minigunPath + "Shoot") as AudioClip,
			Resources.Load(minigunPath + "WindDown") as AudioClip,
		};
	
	}
	
	
    public override void Update () {

        if (!crosshair)
        {
            crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        }

        //Wind-up
        if (Input.GetMouseButton(0))
        {
            windDirection = true;
            if (currentWindUp <= windUpDuration)
            {
                currentWindUp = currentWindUp + Time.deltaTime;
            }
        }
        //Wind-down
        else
        {
            windDirection = false;
            if (currentWindUp > 0)
            {
                currentWindUp = currentWindUp - Time.deltaTime;
            }
            if (currentWindUp < 0)
            {
                currentWindUp = 0;
            }
        }
	
    }

    /// <summary>
    /// Minigun-specific attack
    /// </summary>
    public override void Attack()
    {
        print("Attacked with Minigun");

        if (currentWindUp >= windUpDuration)
        {
            Camera cameraObject = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            GameObject bullet;// = Instantiate(projectile, this.transform.position, this.transform.rotation) as GameObject;
            //bullet.rigidbody.AddForce(bullet.transform.forward * attackSpeed);
            Ray usingRay = cameraObject.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hit;

            //Physics.Raycast(usingRay, out hit, 1000);

            if (Physics.Raycast(usingRay, out hit, 1000))
            {
                //Vector3 rot = transform.rotation.eulerAngles;
                //rot = new Vector3(rot.x, rot.y, rot.z);

                bullet = Instantiate(projectile, bulletOrigin.transform.position, bulletOrigin.transform.rotation) as GameObject;
                bullet.GetComponent<MinigunProjectile>().endPosition = hit.point;
                bullet.GetComponent<MinigunProjectile>().speed = attackSpeed;

                if (hit.collider.gameObject.GetComponent<EnemyScript>())
                {
                    Debug.DrawLine(bulletOrigin.transform.position, hit.point, Color.red);
                    Debug.Log(hit.collider.gameObject.name + " hit!");
                    hit.collider.gameObject.GetComponent<EnemyScript>().IsHit(this);
                }
            }

                //Debug.Log(hit.point + hit.collider.gameObject.name);
                //bullet = Instantiate(projectile, bulletOrigin.transform.position, bulletOrigin.transform.rotation) as GameObject;
                //bullet.GetComponent<ProjectileScript>().weaponOrigin = this;
                //bullet.GetComponent<ProjectileScript>().attackType = 1;
                //bullet.GetComponent<ProjectileScript>().endPosition = hit.point;
                //bullet.rigidbody.velocity = (hit.point - bulletOrigin.transform.position).normalized * attackSpeed;
                //bullet.rigidbody.rotation = Quaternion.LookRotation(bullet.rigidbody.velocity);
                
            //}

            ////Camera cameraObject = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            //Vector3 direction;
            ////Ray usingRay = cameraObject.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            //Ray minigunRay = new Ray(bulletOrigin.transform.position, crosshair.transform.position.normalized);
            //RaycastHit hit;

            ////Check if the ray hits, 10 is the range
            //if (Physics.Raycast(minigunRay, out hit, 1000))
            //{
            //    Debug.DrawLine(minigunRay.origin, hit.point, Color.white);

            //    Debug.Log(hit.collider.gameObject.name + " hit!");

            //    direction = (hit.point - transform.position).normalized;
            //}
            //else
            //{
            //    direction = (crosshair.transform.position - bulletOrigin.transform.position).normalized;
            //}

            //GameObject bullet = Instantiate(projectile, bulletOrigin.transform.position, bulletOrigin.transform.rotation) as GameObject;
            //bullet.GetComponent<ProjectileScript>().weaponOrigin = this;
            //bullet.GetComponent<ProjectileScript>().attackType = 1;
            //bullet.rigidbody.velocity = direction * attackSpeed;
            //bullet.rigidbody.rotation = Quaternion.LookRotation(bullet.rigidbody.velocity);

            //Check if player has a camera component
            //if (this.GetComponentInChildren<Camera>())
            //if (this.GetComponentInParent<Camera>())
            //{
            //    Debug.Log("Camera found!");
            //    Camera cameraObject = this.GetComponentInParent<Camera>();
            //    //Create a raycast from the center of the camera
            //    //Vector is normalized so 0.5F is 50% of the screen size
            //    Ray usingRay = cameraObject.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            //    //Ray usingRay = camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            //    RaycastHit hit;

            //    //Check if the ray hits, 10 is the range
            //    if (Physics.Raycast(usingRay, out hit, 100))
            //    {
            //        Debug.DrawLine(usingRay.origin, hit.point, Color.white);

            //        Debug.Log(hit.collider.gameObject.name + " hit!");
            //        //If it hits, check if the object hit is an enemy
            //if (hit.collider.gameObject.GetComponent<EnemyScript>())
            //{
            //    Debug.Log(hit.collider.gameObject.name + " hit!");
            //    //If it does, call the OpenShop function from the shopscript
            //    hit.collider.gameObject.GetComponent<EnemyScript>().IsHit(this);
            //}
            //    }
            //}

            ammunition--;
            //audio.Play();
            //audio.PlayOneShot(attackSound);
            audio.PlayOneShot(attackSounds[1]);
        }

        if (currentWindUp < windUpDuration)
        {
            if (windDirection == true)
            {
                audio.PlayOneShot(attackSounds[0]);
            }
            else
            {
                audio.PlayOneShot(attackSounds[2]);
            }
        }
    }

    public override void Attack2()
    {
        Debug.Log("Secondary Attack Fired");
    }

}
