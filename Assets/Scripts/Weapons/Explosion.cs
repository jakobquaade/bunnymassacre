﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Explosion : MonoBehaviour
{

    public float explosionPower;
    private Collider[] queuedTargets;
    public WeaponBase weaponOrigin;
    public GameObject explosionEffect;
    

    // Use this for initialization
    void Start()
    {
        Explode();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Explode()
    {
        queuedTargets = Physics.OverlapSphere(this.GetComponent<SphereCollider>().transform.position, this.GetComponent<SphereCollider>().radius);

        foreach (Collider col in queuedTargets)
        {
            if (col.GetComponent<BunnyAI>())
            {
                if(col.GetComponent<EnemyScript>().currentState == EnemyState.bubbled)
                {
                    col.GetComponent<EnemyScript>().currentState = EnemyState.normal;
                }
                col.GetComponent<BunnyAI>().AddKnockback((col.transform.position - this.transform.position).normalized * explosionPower);
                col.GetComponent<AIStats>().Damage(weaponOrigin);
            }
        }

        Instantiate(explosionEffect, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
 }
