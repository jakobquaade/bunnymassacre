﻿using UnityEngine;
using System.Collections;

public class HammerHit : MonoBehaviour
{

    WeaponBase weaponOrigin;

    // Use this for initialization
    void Start()
    {
        weaponOrigin = GameManagerScript.instance.Hammer;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<AIStats>())
        {
            if (!weaponOrigin.GetComponent<HammerScript>().attackReady)
            {
                other.GetComponent<EnemyScript>().IsHit(weaponOrigin);
                Debug.Log(other.gameObject.name + "hit with hammer!");
            }
        }
    }

}
