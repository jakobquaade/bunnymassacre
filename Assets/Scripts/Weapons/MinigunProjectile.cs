﻿using UnityEngine;
using System.Collections;

public class MinigunProjectile : MonoBehaviour
{

    public Vector3 endPosition;
    public float speed;
    public float timeSpawned;
    public float timeAlive;

    // Use this for initialization
    void Start()
    {
        timeSpawned = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - timeSpawned > timeAlive)
        {
            Destroy(this.gameObject);
        }
            
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, endPosition, step);
        
    }
}
