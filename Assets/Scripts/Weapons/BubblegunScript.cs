﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BubblegunScript : WeaponBase
{
    private string bubblegunPath = "Sound/Effects/Weapons/Bubblegun/Bubblegun_";

    public float bubbleLength;
    public float bubbleHeight;
    public float bubblingDistance;
    public float bubbleLiftSpeed;

    public float vortexLength;
    public float vortexHeight;
    public float vortexDistance;
    public float vortexLiftSpeed;

    // Use this for initialization
    void Start()
    {
        crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        itemName = "Bubblegun";
        description = "They all float down here.";

        //Model offset
        modelPosOffset = new Vector3(0.51273f, -0.3754823f, 1.131902f);
        modelRotOffset = Quaternion.Euler(357.4796f, 3.053389f, 359.5378f);


        //Fields 
        //Primary attack
        weaponType = WeaponType.Ranged;
        coolDown = 0.3f;
        range = 50f;
        attackSpeed = 10f;
        damage = 0f;
        origDamage = 0f;
        lastAttack = Time.time;
        maxAmmunition = 30;
        ammunition = 30;
        knockback = 3;
        bubbleLength = 15;
        bubbleHeight = 3;
        bubblingDistance = 1;
        bubbleLiftSpeed = 2;

        //Secondary attack
        weaponType2 = WeaponType.Ranged;
        coolDown2 = 3f;
        range2 = 50f;
        attackSpeed2 = 10f;
        lastAttack2 = Time.time;
        ammunition2 = 10;
        maxAmmunition2 = 10;
        vortexHeight = 3;
        vortexDistance = 1;
        vortexLiftSpeed = 1;

        //Sounds
        attackSounds = new AudioClip[]{
			Resources.Load(bubblegunPath + "Bob") as AudioClip,
			Resources.Load(bubblegunPath + "Hit") as AudioClip,
			Resources.Load(bubblegunPath + "Fire") as AudioClip,
            Resources.Load(bubblegunPath + "Implode") as AudioClip,
            Resources.Load(bubblegunPath + "Pop") as AudioClip,
		};

    }

    // Update is called once per frame
    public override void Update()
    {
        if (!crosshair)
        {
            crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        }
    }

    public override void Attack()
    {
        
        //Camera cameraObject = this.GetComponentInParent<Camera>();
        GameObject bullet;// = Instantiate(projectile, this.transform.position, this.transform.rotation) as GameObject;
        //bullet.rigidbody.AddForce(bullet.transform.forward * attackSpeed);
        //Ray usingRay = cameraObject.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        //RaycastHit hit;

        //if (Physics.Raycast(usingRay, out hit, 1000))
        //{
        //    Debug.DrawLine(transform.position, hit.point, Color.red);

        //    Debug.Log(hit.point);
        //    bullet = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
        //    bullet.GetComponent<ProjectileScript>().weaponOrigin = this;
        //    bullet.GetComponent<ProjectileScript>().attackType = 1;
        //    bullet.rigidbody.velocity = (hit.point - transform.position).normalized * attackSpeed;
        //    bullet.rigidbody.rotation = Quaternion.LookRotation(bullet.rigidbody.velocity);

        //}


        bullet = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
        bullet.GetComponent<ProjectileScript>().weaponOrigin = this;
        bullet.GetComponent<ProjectileScript>().attackType = 1;
        bullet.rigidbody.velocity = (crosshair.transform.position - transform.position).normalized * attackSpeed;
        bullet.rigidbody.rotation = Quaternion.LookRotation(bullet.rigidbody.velocity);

        audio.PlayOneShot(attackSounds[2]);

    }

    public override void Attack2()
    {
        Debug.Log("Secondary Attack Fired");

        //Camera cameraObject = this.GetComponentInParent<Camera>();
        GameObject bullet;// = Instantiate(projectile, this.transform.position, this.transform.rotation) as GameObject;
        //bullet.rigidbody.AddForce(bullet.transform.forward * attackSpeed);
        //Ray usingRay = cameraObject.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        //RaycastHit hit;

        //if (Physics.Raycast(usingRay, out hit, 1000))
        //{
        //    Debug.DrawLine(transform.position, hit.point, Color.red);

        //    Debug.Log(hit.point);
        //    bullet = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
        //    bullet.GetComponent<ProjectileScript>().weaponOrigin = this;
        //    bullet.GetComponent<ProjectileScript>().attackType = 2;
        //    bullet.rigidbody.velocity = (hit.point - transform.position).normalized * attackSpeed;
        //    bullet.rigidbody.rotation = Quaternion.LookRotation(bullet.rigidbody.velocity);

        //}

        bullet = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
        bullet.GetComponent<ProjectileScript>().weaponOrigin = this;
        bullet.GetComponent<ProjectileScript>().attackType = 2;
        bullet.rigidbody.velocity = (crosshair.transform.position - transform.position).normalized * attackSpeed;
        bullet.rigidbody.rotation = Quaternion.LookRotation(bullet.rigidbody.velocity);

        audio.PlayOneShot(attackSounds[2]);
    }
}
