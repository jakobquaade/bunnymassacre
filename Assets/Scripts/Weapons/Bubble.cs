﻿using UnityEngine;
using System.Collections;

public class Bubble : MonoBehaviour
{

    public GameObject target;

    // Update is called once per frame
    void Update()
    {
        if (target.GetComponent<EnemyScript>().currentState == EnemyState.bubbled)
            transform.position = new Vector3(target.transform.position.x, target.transform.position.y + 0.75f, target.transform.position.z);
        else
            Destroy(this.gameObject);

    }
}
