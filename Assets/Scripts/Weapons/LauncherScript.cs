﻿using UnityEngine;
using System.Collections;

public class LauncherScript : WeaponBase
{
    private string launcherPath = "Sound/Effects/Weapons/Launcher/Launcher_";
    public float explosionSize;
    public float explosionPower;
    public float explosionSize2;
    public float explosionPower2;

    // Use this for initialization
    void Start()
    {
        crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        itemName = "Launcher";
        description = "When you need to blow something up, but you just can't reach it.";

        //Model offset
        modelPosOffset = new Vector3(0.5051894f, -0.3498303f, 1.279101f);
        modelRotOffset = Quaternion.Euler(270f, 180.2163f, 0f);


        //Fields 
        //Primary attack
        weaponType = WeaponType.Ranged;
        coolDown = 2f;
        range = 50f;
        attackSpeed = 30f;
        damage = 100f;
        lastAttack = Time.time;
        maxAmmunition = 10;
        ammunition = 10;
        knockback = 2000;
        explosionSize = 20;
        explosionPower = 2000;

        //Secondary attack
        weaponType2 = WeaponType.Ranged;
        coolDown2 = 1.5f;
        range2 = 50f;
        attackSpeed2 = 10f;
        damage2 = 100f;
        lastAttack2 = Time.time;
        ammunition2 = 10;
        maxAmmunition2 = 10;
        knockback2 = 3;

        //Sounds
        attackSounds = new AudioClip[]{
			Resources.Load(launcherPath + "Bob") as AudioClip,
			Resources.Load(launcherPath + "Explosion") as AudioClip,
			Resources.Load(launcherPath + "Fire") as AudioClip,
            Resources.Load(launcherPath + "Minebeep") as AudioClip,
            Resources.Load(launcherPath + "MineThump") as AudioClip,
		};

    }

    // Update is called once per frame
    public override void Update()
    {

    }

    public override void Attack()
    {
        crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        Camera cameraObject = this.GetComponentInParent<Camera>();
        GameObject bullet;// = Instantiate(projectile, this.transform.position, this.transform.rotation) as GameObject;
        //bullet.rigidbody.AddForce(bullet.transform.forward * attackSpeed);
        Ray usingRay = cameraObject.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;

        if (Physics.Raycast(usingRay, out hit, 1000))
        {
             //Vector3 rot = transform.rotation.eulerAngles;
             //rot = new Vector3(rot.x, rot.y, rot.z);

            Debug.DrawLine(transform.position, hit.point, Color.red);

            Debug.Log(hit.point);
            bullet = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
            bullet.GetComponent<ProjectileScript>().weaponOrigin = this;
            bullet.GetComponent<ProjectileScript>().attackType = 1;
            bullet.rigidbody.velocity = (hit.point - transform.position).normalized * attackSpeed;
            bullet.rigidbody.rotation = Quaternion.LookRotation(bullet.rigidbody.velocity);
            audio.PlayOneShot(attackSounds[2]);
        }
    }

    public override void Attack2()
    {
        Camera cameraObject = this.GetComponentInParent<Camera>();
        GameObject bullet;// = Instantiate(projectile, this.transform.position, this.transform.rotation) as GameObject;
        //bullet.rigidbody.AddForce(bullet.transform.forward * attackSpeed);
        Ray usingRay = cameraObject.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;

        if (Physics.Raycast(usingRay, out hit, 1000))
        {
            Debug.DrawLine(transform.position, hit.point, Color.red);

            Debug.Log(hit.point);
            bullet = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
            bullet.GetComponent<ProjectileScript>().weaponOrigin = this;
            bullet.GetComponent<ProjectileScript>().attackType = 2;
            bullet.rigidbody.velocity = (hit.point - transform.position).normalized * attackSpeed;
            bullet.rigidbody.rotation = Quaternion.LookRotation(bullet.rigidbody.velocity);
            audio.PlayOneShot(attackSounds[2]);
        }
        Debug.Log("Secondary Attack Fired");
    }
}
