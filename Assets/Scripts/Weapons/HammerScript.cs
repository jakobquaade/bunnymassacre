﻿using UnityEngine;
using System.Collections;

public class HammerScript : WeaponBase {

    private string hammerPath = "Sound/Effects/Weapons/Hammer/Hammer_";
    public Animator animator;
    public bool attackReady;
    private SphereCollider attackCollider;

	// Use this for initialization
	void Start () {

        attackCollider = GetComponentInChildren<SphereCollider>();

        attackReady = true;
        animator = GetComponentInChildren<Animator>();
        
        itemName = "Hammer";
        description = "A sturdy hammer capable of demolishing buildings. Also demolishes bunnies.";
        

        //Position and Rotation of the model relative to the camera
        modelPosOffset = new Vector3(1.022999f, -0.2036401f, 1.862f);
        modelRotOffset = Quaternion.Euler(3.575499f, 173.8525f, 6.915502f);

        //Fields 
        //Primary attack
        weaponType = WeaponType.Melee;
        coolDown = 0.5f;
        range = 1f;
        attackSpeed = 1f;
        damage = 100f;
        origDamage = 100f;
        lastAttack = Time.time;
        knockback = 1000f;

        //Secondary attack
        weaponType2 = WeaponType.Melee;
        coolDown2 = 1f;
        range2 = 1f;
        attackSpeed2 = 1f;
        damage2 = 100f;
        origDamage2 = 100f;
        lastAttack2 = Time.time;


        attackSounds = new AudioClip[]{
			Resources.Load(hammerPath + "Swing1") as AudioClip,
			Resources.Load(hammerPath + "Swing2") as AudioClip,
			Resources.Load(hammerPath + "Swing3") as AudioClip,
			Resources.Load(hammerPath + "Hit1") as AudioClip,
			Resources.Load(hammerPath + "Hit2") as AudioClip,
			Resources.Load(hammerPath + "Hit3") as AudioClip
		};


	}
	
    public override void Update () {

        

        //GetComponent<CapsuleCollider>().transform.localRotation = GetComponentInChildren<Transform>().transform.localRotation;

        if (Time.time > lastAttack + coolDown && !attackReady)
        {
            animator.SetBool("Attack", false);
            attackReady = true;
        }
        
        
    }

    /// <summary>
    /// Hammer-specific attack
    /// </summary>
    public override void Attack()
    {
       print("Attacked with Hammer");
       if (animator.GetBool("Attack") == false)
       {
           animator.SetBool("Attack", true);
           attackReady = false;
       }
       //audio.PlayOneShot(attackSound);
       int swingsound = Random.Range(0, 3);
       audio.PlayOneShot(attackSounds[swingsound]);

    }
    public override void Attack2()
    {
        int swingsound = Random.Range(0, 3);
        audio.PlayOneShot(attackSounds[swingsound]);
        Debug.Log("Secondary Attack Fired");
    }
}
