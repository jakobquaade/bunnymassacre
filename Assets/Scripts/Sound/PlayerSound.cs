﻿using UnityEngine;
using System.Collections;

public class PlayerSound : MonoBehaviour
{

    public AudioClip[] playerSounds;
    private string soundPath = "Sound/Effects/Player/Player_";
    float timer = 0.0f;
    float timerMax = 0.1f;

    // Use this for initialization
    void Start()
    {

        playerSounds = new AudioClip[]{
			Resources.Load(soundPath + "Footstep") as AudioClip,
			Resources.Load(soundPath + "Jump") as AudioClip,
			Resources.Load(soundPath + "Land") as AudioClip,
			Resources.Load(soundPath + "LooseCarrot") as AudioClip,
            Resources.Load("Sound/Effects/Weapons/Change_weapon") as AudioClip
			};

        timerMax = playerSounds[0].length;



    }

    // Update is called once per frame
    void Update()
    {



        //Footsteps
        if (Input.GetKey("w") || Input.GetKey("a") || Input.GetKey("s") || Input.GetKey("d"))
        {
            timer += Time.deltaTime;
            if (timer >= timerMax)
            {
                timer = 0.0f;
                audio.PlayOneShot(playerSounds[0]);
            }
        }

        //Jumping
        if (Input.GetKeyDown("space"))
        {
            audio.PlayOneShot(playerSounds[1]);
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            audio.PlayOneShot(playerSounds[3]);
        }


    }
}