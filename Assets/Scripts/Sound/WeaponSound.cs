﻿using UnityEngine;
using System.Collections;

public class WeaponSound : MonoBehaviour
{
    private int weapon = 1;

    //Minigun
    float minigunTimer = 0.0f;
    float minigunTimerMax = 0.10f; //minigun firerate
    //float minigunWindTimer = 0.0f;

    //int minigunFire;
    private AudioClip[] minigun;
    private string minigunPath = "Sound/Effects/Weapons/Minigun/Minigun_";

    //Hammer
    private AudioClip[] hammer;
    private string hammerPath = "Sound/Effects/Weapons/Hammer/Hammer_";
    int swingsound;




    // Use this for initialization
    void Start()
    {


        // Minigun Sounds
        minigun = new AudioClip[]{
			Resources.Load(minigunPath + "WindUp") as AudioClip,
			Resources.Load(minigunPath + "Shoot") as AudioClip,
			Resources.Load(minigunPath + "WindDown") as AudioClip,
			Resources.Load("Sound/Effects/Weapons/Change_weapon") as AudioClip
		};

        // Hammer Sounds
        hammer = new AudioClip[]{
			Resources.Load(hammerPath + "Swing1") as AudioClip,
			Resources.Load(hammerPath + "Swing2") as AudioClip,
			Resources.Load(hammerPath + "Swing3") as AudioClip,
			Resources.Load(hammerPath + "Hit1") as AudioClip,
			Resources.Load(hammerPath + "Hit2") as AudioClip,
			Resources.Load(hammerPath + "Hit3") as AudioClip
		};

    }

    // Update is called once per frame
    void Update()
    {
        // Weapon Select
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            weapon = 1;
            audio.PlayOneShot(minigun[3]);
            Debug.Log(weapon);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            weapon = 2;
            audio.PlayOneShot(minigun[3]);
            Debug.Log(weapon);
        }

        // Hammer
        if (Input.GetKeyDown(KeyCode.Mouse0) && weapon == 1)
        {
            swingsound = Random.Range(0, 3);
            audio.PlayOneShot(hammer[swingsound]);
        }



        // Minigun

        if (Input.GetKey(KeyCode.Mouse0) && weapon == 2)
        {

            minigunTimer += Time.deltaTime;
            if (minigunTimer >= minigunTimerMax)
            {
                minigunTimer = 0.0f;
                audio.PlayOneShot(minigun[1]);

            }

        }

        if (Input.GetKeyUp(KeyCode.Mouse0) && weapon == 2)
        {
            audio.PlayOneShot(minigun[2]);
            //minigunWindTimer = 0.0f;
            //minigunFire = 0;
        }

    }
}