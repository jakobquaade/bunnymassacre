﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum MenuState { Main, Options, HighScore, InGame, None };

public class MenuManager : MonoBehaviour
{

    #region Fields
    //static MenuManager singleton
    private static MenuManager _instance;

    MenuState currentMenu;
    MenuState lastMenu;
    public GameObject MainMenuCanvas;
    public GameObject HighScoreCanvas;
    public GameObject OptionsCanvas;
    public GameObject InGameCanvas;
    private GameObject mainMenuGO;
    private GameObject highScoreGO;
    private GameObject optionsGO;
    private GameObject inGameGO;
    private Button startBtn;
    private Button highScoreBtn;
    private Button optionsBtn;
    private Button backHIBtn;
    private Button exitBtn;
    private Button resumeBtn;
    private Button highScoreIGBtn;
    private Button optionsIGBtn;
    private Button backIGBtn;
    private Button backOPBtn;
    private Text First, Second, Third, Fourth, Fifth, Sixth, Seventh, Eighth, Ninth, Tenth;
    private Slider masterSlider, musicSlider, soundFXSlider;
    private bool gameIsStarted = false;

    #endregion

    #region Constructor, Awake, Start, Update, OnGUI
    //Constructor
    public static MenuManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<MenuManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);

        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    void Start()
    {
        Time.timeScale = 0;
        lastMenu = MenuState.None;
        currentMenu = MenuState.Main;
    }

    void Update()
    {
        if (Time.timeScale == 1 && Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0;
            Debug.Log("Game paused");
            if (GameManagerScript.instance.player == null)
            {
                GameManagerScript.instance.player = GameObject.FindGameObjectWithTag("Player");
            }
            currentMenu = MenuState.InGame;
            lastMenu = MenuState.None;
        }
    }

    void OnGUI()
    {
        Debug.Log("currentMenu = " + currentMenu + ", lastMenu = " + lastMenu);
        switch (currentMenu)
        {
            case MenuState.Main:
                if (lastMenu != MenuState.None)
                {
                    DisableLastMenu();
                }
                RenderMainMenu();
                break;

            case MenuState.Options:
                DisableLastMenu();
                RenderOptionsMenu();
                break;

            case MenuState.HighScore:
                Debug.Log("OnGUI, Highscore");
                DisableLastMenu();
                RenderHighScoreMenu();
                break;

            case MenuState.InGame:
                DisableLastMenu();
                RenderInGameMenu();
                break;

            case MenuState.None:
                DisableAllMenus();
                break;

            default:
                break;
        }
    }
    #endregion

    #region RenderMenu-functions
    void RenderMainMenu()
    {
        if (mainMenuGO == null)
        {
            mainMenuGO = Instantiate(MainMenuCanvas) as GameObject;
        }
        else
        {
            mainMenuGO.GetComponent<Canvas>().enabled = true;
        }
        //Tildeler knapperne deres OnClick-funktioner.
        if (startBtn == null)
        {
            startBtn = GameObject.Find("StartBtn").GetComponent<Button>();
            startBtn.onClick.AddListener(() => { StartGame(); });
        }
        //else
        //{
        //    Debug.Log("StartBtn could not be found, no function assigned to it.");
        //}

        if (highScoreBtn == null)
        {
            highScoreBtn = GameObject.Find("HighScoreBtn").GetComponent<Button>();
            highScoreBtn.onClick.AddListener(() => { HighScore(); });
        }
        //else
        //{
        //    Debug.Log("HighScoreBtn could not be found, no function assigned to it.");
        //}


        if (optionsBtn == null)
        {
            optionsBtn = GameObject.Find("OptionsBtn").GetComponent<Button>();
            optionsBtn.onClick.AddListener(() => { Options(); });
        }
        //else
        //{
        //    Debug.Log("OptionsBtn could not be found, no function assigned to it.");
        //}

        if (exitBtn == null)
        {
            exitBtn = GameObject.Find("ExitBtn").GetComponent<Button>();
            exitBtn.onClick.AddListener(() => { ExitGame(); });
        }
        //else
        //{
        //    Debug.Log("HighScoreBtn could not be found, no function assigned to it.");
        //}
    }

    void RenderHighScoreMenu()
    {
        if (highScoreGO == null)
        {
            highScoreGO = Instantiate(HighScoreCanvas) as GameObject;
        }
        else
        {
            highScoreGO.GetComponent<Canvas>().enabled = true;
        }

        backHIBtn = GameObject.Find("BackToMainBtn").GetComponent<Button>();
        if (backHIBtn != null)
        {
            backHIBtn.onClick.AddListener(() => { MainMenuFromHI(); });
        }
        else
        {
            Debug.Log("BackBtn could not be found, no function assigned to it.");
        }


        Config.CheckScore();
        if (First == null)
        {
            First = GameObject.Find("1stPlace").GetComponent<Text>();
            Second = GameObject.Find("2ndPlace").GetComponent<Text>();
            Third = GameObject.Find("3rdPlace").GetComponent<Text>();
            Fourth = GameObject.Find("4thPlace").GetComponent<Text>();
            Fifth = GameObject.Find("5thPlace").GetComponent<Text>();
            Sixth = GameObject.Find("6thPlace").GetComponent<Text>();
            Seventh = GameObject.Find("7thPlace").GetComponent<Text>();
            Eighth = GameObject.Find("8thPlace").GetComponent<Text>();
            Ninth = GameObject.Find("9thPlace").GetComponent<Text>();
            Tenth = GameObject.Find("10thPlace").GetComponent<Text>();
        }


        First.text = "1.   " + Config.Highscores[0].ToString();
        Second.text = "2.   " + Config.Highscores[1].ToString();
        Third.text = "3.   " + Config.Highscores[2].ToString();
        Fourth.text = "4.   " + Config.Highscores[3].ToString();
        Fifth.text = "5.   " + Config.Highscores[4].ToString();
        Sixth.text = "6.   " + Config.Highscores[5].ToString();
        Seventh.text = "7.   " + Config.Highscores[6].ToString();
        Eighth.text = "8.   " + Config.Highscores[7].ToString();
        Ninth.text = "9.   " + Config.Highscores[8].ToString();
        Tenth.text = "10. " + Config.Highscores[9].ToString();



    }

    void RenderOptionsMenu()
    {
        if (optionsGO == null)
        {
            optionsGO = Instantiate(OptionsCanvas) as GameObject;

            //Load sliders and audio sources
            masterSlider = GameObject.Find("MasterSlider").GetComponent<Slider>();
            masterSlider.maxValue = 1;
            musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
            musicSlider.maxValue = 1;
            soundFXSlider = GameObject.Find("SoundFXSlider").GetComponent<Slider>();
            soundFXSlider.maxValue = 1;

            
            //Assign values to sliders from config file
            masterSlider.value = Config.MasterVolume;
            musicSlider.value = Config.MusicVolume;
            soundFXSlider.value = Config.SoundFXVolume;
        }
        else
        {
            optionsGO.GetComponent<Canvas>().enabled = true;
        }

        if (backOPBtn == null)
        {
            backOPBtn = GameObject.Find("BackToMainOPBtn").GetComponent<Button>();
            backOPBtn.onClick.AddListener(() => { MainMenuFromOP(); });
        }

    }

    void RenderInGameMenu()
    {
        //GameManagerScript.instance.player.GetComponent<PlayerMovement>().enabled = false;

        if (inGameGO == null)
        {
            inGameGO = Instantiate(InGameCanvas) as GameObject;
        }
        else
        {
            inGameGO.GetComponent<Canvas>().enabled = true;
        }

        if (backIGBtn == null)
        {
            backIGBtn = GameObject.Find("BackToMainIGBtn").GetComponent<Button>();
            backIGBtn.onClick.AddListener(() => { MainMenuFromIG(); });
        }
        //else
        //{
        //    Debug.Log("BackToMainIGBtn could not be found, no function assigned to it.");
        //}

        if (highScoreIGBtn == null)
        {
            highScoreIGBtn = GameObject.Find("HighScoreIGBtn").GetComponent<Button>();
            highScoreIGBtn.onClick.AddListener(() => { HighScore(); });
        }
        //else
        //{
        //    Debug.Log("HighScoreIGBtn could not be found, no function assigned to it.");
        //}



        if (optionsIGBtn == null)
        {
            optionsIGBtn = GameObject.Find("OptionsIGBtn").GetComponent<Button>();
            optionsIGBtn.onClick.AddListener(() => { Options(); });
        }
        //else
        //{
        //    Debug.Log("OptionsIGBtn could not be found, no function assigned to it.");
        //}

        if (resumeBtn == null)
        {
            resumeBtn = GameObject.Find("ResumeGameBtn").GetComponent<Button>();
            resumeBtn.onClick.AddListener(() => { ResumeGame(); });
        }
        //else
        //{
        //    Debug.Log("ResumeGameBtn could not be found, no function assigned to it.");
        //}


    }
    #endregion

    #region Button OnClick-functions
    public void StartGame()
    {
        lastMenu = MenuState.Main;
        currentMenu = MenuState.None;
        gameIsStarted = true;
        //Application.LoadLevel(1);
        Debug.Log("Started game");
        Time.timeScale = 1;
    }

    void Options()
    {
        if (currentMenu == MenuState.Main)
        {
            lastMenu = MenuState.Main;
        }
        else if (currentMenu == MenuState.InGame)
        {
            lastMenu = MenuState.InGame;
        }
        currentMenu = MenuState.Options;
    }

    void HighScore()
    {
        Debug.Log("Pressed Highscore button");
        if (currentMenu == MenuState.Main)
        {
            lastMenu = MenuState.Main;
        }
        else if (currentMenu == MenuState.InGame)
        {
            lastMenu = MenuState.InGame;
        }
        currentMenu = MenuState.HighScore;
    }

    void ExitGame()
    {
        Application.Quit();
    }

    void MainMenuFromHI()
    {
        Debug.Log("Back button pressed");

        lastMenu = MenuState.HighScore;

        //Hvis spillet er startet, vender man tilbage til ingame
        //Hvis ikke, kommer man til Main
        if (gameIsStarted)
        {
            currentMenu = MenuState.InGame;
        }
        else
        {
            currentMenu = MenuState.Main;
            gameIsStarted = false;
        }
    }
    void MainMenuFromOP()
    {
        Debug.Log("Back button pressed");
        lastMenu = MenuState.Options;
        Config.MasterVolume = masterSlider.value;
        Config.SoundFXVolume = soundFXSlider.value;
        Config.MusicVolume = musicSlider.value;
        Config.UpdateConfig();

        //Hvis spillet er startet, vender man tilbage til ingame
        //Hvis ikke, kommer man til Main
        if (gameIsStarted)
        {
            currentMenu = MenuState.InGame;
        }
        else
        {
            currentMenu = MenuState.Main;
            gameIsStarted = false;
        }
    }
    void MainMenuFromIG()
    {
        lastMenu = MenuState.InGame;
        currentMenu = MenuState.Main;
        gameIsStarted = false;

    }

    void ResumeGame()
    {
        lastMenu = MenuState.InGame;
        currentMenu = MenuState.None;
        Time.timeScale = 1;
        GameManagerScript.instance.player.GetComponent<PlayerMovement>().enabled = true;

    }
    #endregion

    void DisableAllMenus()
    {
        if (mainMenuGO != null)
        {
            mainMenuGO.GetComponent<Canvas>().enabled = false;
        }
        if (highScoreGO != null)
        {
            highScoreGO.GetComponent<Canvas>().enabled = false;
        }
        if (optionsGO != null)
        {
            optionsGO.GetComponent<Canvas>().enabled = false;
        }
        if (inGameGO != null)
        {
            inGameGO.GetComponent<Canvas>().enabled = false;
        }
    }

    void DisableLastMenu()
    {
        switch (lastMenu)
        {
            case MenuState.Main:
                if (mainMenuGO != null)
                {
                    mainMenuGO.GetComponent<Canvas>().enabled = false;
                }
                break;

            case MenuState.Options:
                if (optionsGO != null)
                {
                    optionsGO.GetComponent<Canvas>().enabled = false;
                }
                break;


            case MenuState.HighScore:
                if (highScoreGO != null)
                {
                    highScoreGO.GetComponent<Canvas>().enabled = false;
                    Debug.Log("Highscore Canvas disabled");
                }
                break;

            case MenuState.InGame:
                if (inGameGO != null)
                {
                    inGameGO.GetComponent<Canvas>().enabled = false;
                }
                break;


            default:
                break;
        }
    }

}
