﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraShake : MonoBehaviour {

	public Vector3 baseLocalPosition;
		
	private float magnitude; //How much to shake.
	private bool isSmoothed; //if true, the max magnitude and rotation is linearly reduced to the minimun magnitude and rotation, over the duration.
	
	private float duration; //how long the shake should be running, in seconds.
	private float curDuration;
	public bool isShaking = false;

	private Vector3 originPos;

	void Awake()
	{
		originPos = transform.localPosition;

	}

	public void AddShake(float mag, bool isSmooth, float dur)
	{
		//Only override the shake, if it's more powerful than the one already running
		if(isSmoothed)
		{
			//We need to check how big it is right now, if its smoothed.
			if(magnitude*(curDuration/duration) < mag)
			{
				magnitude = mag;
				isSmoothed = isSmooth;
				duration = dur;
				curDuration = dur;
				isShaking = true;
			}
		}
		else
		{
			if(magnitude < mag)
			{
				magnitude = mag;
				isSmoothed = isSmooth;
				duration = dur;
				curDuration = dur;
				isShaking = true;
			}
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if(isShaking)
		{
			Shake();
		}
	}

	void Shake()
	{
		//Generate the random values.
		Vector3 rndPosition = new Vector3(Random.Range(0, magnitude), Random.Range(0, magnitude), Random.Range(0, magnitude));
		if(isSmoothed)
		{
			//To smooth it, we divide the values by curDur/Dur, to reduce them as time goes on.
			rndPosition *= curDuration/duration;
			transform.localPosition = originPos + rndPosition;
			curDuration -= Time.deltaTime;
			//Kill the shake if it's out of duration.
			if(curDuration <= 0)
			{
				StopShake();
			}
		}
		else
		{
			//If there's no smooth, we just apply them.
			transform.localPosition = originPos + rndPosition;
			curDuration -= Time.deltaTime;
			//Kill the shake if it's out of duration.
			if(curDuration <= 0)
			{
				StopShake();
			}
		}
	}

	void StopShake()
	{
		transform.localPosition = originPos;
		isShaking = false;
	}
}
