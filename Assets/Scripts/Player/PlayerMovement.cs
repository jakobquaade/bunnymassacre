﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovement : MonoBehaviour {

	public float maxForwardSpeed;
    public float origMaxForwardSpeed;
	public float maxBackwardSpeed;
    public float origMaxBackwardSpeed;
	public float maxSideSpeed;
    public float origMaxSideSpeed;

	public float jumpForce;
	public float gravity;
	public float maxFallSpeed;

	public float acceleration;
	public float stopSlide;

	public float sensitivityX;
	public float sensitivityY;
	public float cameraMinY;
	public float cameraMaxY;

	public GameObject cameraObject;

	public Vector3 movementVector = Vector3.zero;

	public CharacterController controller;
	public CameraBob cameraBob;

	private float moveZ = 0;
	private float moveX = 0;
	private float moveY = 0;

	float rotationY = 0;
	bool jumpedThisFrame = false;

	void Awake()
	{
		controller = gameObject.GetComponent<CharacterController>();
        GameManagerScript.instance.player = this.gameObject;
        Screen.showCursor = false;
	}

	// Update is called once per frame
	void Update() 
	{
        
            

		//movement
		UpdateMovement();
		RotateCamAndPlayer();
        ChangeWeapon();
	}

	void UpdateMovement()
	{
		//Reset the jump variable.
		jumpedThisFrame = false;
		//reset camera state
		cameraBob.state = State.Standing;


		//Right
		if (Input.GetAxisRaw("Horizontal") > 0)
		{
			moveX = Mathf.Clamp(moveX+acceleration*Time.deltaTime,maxSideSpeed*-1,maxSideSpeed);
			cameraBob.state = State.RightRun;
		}

		//Left
		if (Input.GetAxisRaw("Horizontal") < 0)
		{
			moveX = Mathf.Clamp(moveX-acceleration*Time.deltaTime,maxSideSpeed*-1,maxSideSpeed);
			cameraBob.state = State.LeftRun;
		}

		//Backwards movement
		if (Input.GetAxisRaw("Vertical") < 0)
		{
			moveZ = Mathf.Clamp(moveZ-acceleration*Time.deltaTime,maxBackwardSpeed,maxForwardSpeed);
			cameraBob.state = State.BackwardRun;
		}

		//Forward movement
		if (Input.GetAxisRaw("Vertical") > 0)
		{
			moveZ = Mathf.Clamp(moveZ+acceleration*Time.deltaTime,maxBackwardSpeed,maxForwardSpeed);
			cameraBob.state = State.ForwardRun;
		}

        //Stopping movement
        //Stop horizontal movement
        if (Input.GetAxisRaw("Horizontal") == 0)
        {
            moveX = Mathf.Lerp(moveX, 0, Time.time * stopSlide);
            if (moveX < 0.1 && moveX > -0.1) { moveX = 0; }
        }
        //Vertical
        if (Input.GetAxisRaw("Vertical") == 0)
        {
            moveZ = Mathf.Lerp(moveZ, 0, Time.time * stopSlide);
            if (moveZ < 0.1 && moveZ > -0.1) { moveZ = 0; }
        }

        //Jumping
        if (Input.GetButton("Jump") && controller.isGrounded)
        {
            jumpedThisFrame = true;
            moveY = jumpForce;
        }

        //Gravity
        if (!controller.isGrounded)
        {
            moveY = Mathf.Clamp(moveY + gravity * Time.deltaTime, maxFallSpeed, maxFallSpeed * -1);
            //cameraBob.state = State.Jumping;
        }
        //Stop if we hit the ground, but only if we didn't jump this frame. Otherwise we would never jump.
        if (controller.isGrounded && !jumpedThisFrame)
        {
            moveY = 0;
        }


        movementVector = new Vector3(moveX, 0, moveZ);
        movementVector = transform.TransformDirection(movementVector);

        movementVector = new Vector3(movementVector.x, moveY, movementVector.z);

        //movementVector = movementVector*Time.deltaTime;

        //Move the player!
        controller.Move(movementVector * Time.deltaTime);
    }

    void RotateCamAndPlayer()
    {
        //Rotate body
        transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);

        //Rotate camera
        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        rotationY = Mathf.Clamp(rotationY, cameraMinY, cameraMaxY);

        cameraObject.transform.localEulerAngles = new Vector3(-rotationY, 0, 0);
    }


    void ChangeWeapon()
    {
        //Switching weapons on 1-9
        //Hammer
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            //If no weapon is currently equipped
            if (GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon == null)
            {
                GameManagerScript.instance.player.GetComponent<Weapon>().SwitchWeapon(GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon);
                //Set current weapon to selected weapon
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon = GameManagerScript.instance.Hammer;
                //Set parent of weapon to main camera
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindGameObjectWithTag("WeaponCamera").transform;
                //Set local position and rotation of selected weapon
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelPosOffset;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localRotation = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelRotOffset;
                
                MeshRenderer[] tempList;
                tempList = GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer x in tempList)
                {
                    x.renderer.enabled = true;
                }
                if (GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponent<Animator>())
                    GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponent<Animator>().enabled = true;
            }
            //If current weapon is not equal to the selected weapon
            else if (GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon != GameManagerScript.instance.Hammer)
            {

                GameManagerScript.instance.player.GetComponent<Weapon>().SwitchWeapon(GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon);
                //Set parent of current weapon to the game manager instead, making room for new current weapon
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindObjectOfType<GameManagerScript>().transform;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindObjectOfType<GameManagerScript>().transform.position;
                //Set current weapon to selected weapon
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon = GameManagerScript.instance.Hammer;
                //Set parent of weapon to main camera
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindGameObjectWithTag("WeaponCamera").transform;
                //Set local position and rotation of selected weapon
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelPosOffset;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localRotation = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelRotOffset;
                
                MeshRenderer[] tempList;
                tempList = GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer x in tempList)
                {
                    x.renderer.enabled = true;
                }
                if (GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponent<Animator>())
                    GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponent<Animator>().enabled = true;
            }
            GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        }
        //Minigun
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (GameManagerScript.instance.ownedWeapons.Contains(GameManagerScript.instance.Minigun))
            {
                //If no weapon is currently equipped
                if (GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon == null)
                {
                    GameManagerScript.instance.player.GetComponent<Weapon>().SwitchWeapon(GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon);
                    //Set current weapon to selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon = GameManagerScript.instance.Minigun;
                    //GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.renderer.enabled = true;
                    //Set parent of weapon to main camera
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindGameObjectWithTag("WeaponCamera").transform;
                    //Set local position and rotation of selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelPosOffset;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localRotation = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelRotOffset;

                    MeshRenderer[] tempList;
                    tempList = GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponentsInChildren<MeshRenderer>();
                    foreach (MeshRenderer x in tempList)
                    {
                        x.renderer.enabled = true;
                    }
                }
                //If current weapon is not equal to the selected weapon
                else if (GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon != GameManagerScript.instance.Minigun || GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon == null)
                {
                    GameManagerScript.instance.player.GetComponent<Weapon>().SwitchWeapon(GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon);
                    //Set parent of current weapon to the game manager instead, making room for new current weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindObjectOfType<GameManagerScript>().transform;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindObjectOfType<GameManagerScript>().transform.position;
                    //Set current weapon to selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon = GameManagerScript.instance.Minigun;
                    //Set parent of weapon to main camera
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindGameObjectWithTag("WeaponCamera").transform;
                    //Set local position and rotation of selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelPosOffset;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localRotation = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelRotOffset;
                    MeshRenderer[] tempList;
                    tempList = GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponentsInChildren<MeshRenderer>();
                    foreach (MeshRenderer x in tempList)
                    {
                        x.renderer.enabled = true;
                    }
                }

                GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.crosshair = GameObject.FindGameObjectWithTag("Crosshair");
            }
        }
        //Bubblegun
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (GameManagerScript.instance.ownedWeapons.Contains(GameManagerScript.instance.Bubblegun))
            {
                //If no weapon is currently equipped
                if (GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon == null)
                {
                    GameManagerScript.instance.player.GetComponent<Weapon>().SwitchWeapon(GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon);
                    //Set current weapon to selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon = GameManagerScript.instance.Bubblegun;
                    //Set parent of weapon to main camera
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindGameObjectWithTag("WeaponCamera").transform;
                    //Set local position and rotation of selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelPosOffset;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localRotation = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelRotOffset;
                    //GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.renderer.enabled = true;
                    MeshRenderer[] tempList;
                    tempList = GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponentsInChildren<MeshRenderer>();
                    foreach (MeshRenderer x in tempList)
                    {
                        x.renderer.enabled = true;
                    }
                }
                //If current weapon is not equal to the selected weapon
                else if (GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon != GameManagerScript.instance.Bubblegun || GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon == null)
                {
                    GameManagerScript.instance.player.GetComponent<Weapon>().SwitchWeapon(GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon);
                    //Set parent of current weapon to the game manager instead, making room for new current weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindObjectOfType<GameManagerScript>().transform;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindObjectOfType<GameManagerScript>().transform.position;
                    //Set current weapon to selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon = GameManagerScript.instance.Bubblegun;
                    //Set parent of weapon to main camera
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindGameObjectWithTag("WeaponCamera").transform;
                    //Set local position and rotation of selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelPosOffset;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localRotation = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelRotOffset;

                    MeshRenderer[] tempList;
                    tempList = GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponentsInChildren<MeshRenderer>();
                    foreach (MeshRenderer x in tempList)
                    {
                        x.renderer.enabled = true;
                    }
                }

                GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.crosshair = GameObject.FindGameObjectWithTag("Crosshair");
            }
        }

        //Launcher
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (GameManagerScript.instance.ownedWeapons.Contains(GameManagerScript.instance.Launcher))
            {
                //If no weapon is currently equipped
                if (GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon == null)
                {
                    GameManagerScript.instance.player.GetComponent<Weapon>().SwitchWeapon(GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon);
                    //Set current weapon to selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon = GameManagerScript.instance.Launcher;
                    GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.renderer.enabled = true;
                    //Set parent of weapon to main camera
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindGameObjectWithTag("WeaponCamera").transform;
                    //Set local position and rotation of selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelPosOffset;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localRotation = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelRotOffset;
                }
                //If current weapon is not equal to the selected weapon
                else if (GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon != GameManagerScript.instance.Launcher || GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon == null)
                {
                    GameManagerScript.instance.player.GetComponent<Weapon>().SwitchWeapon(GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon);
                    //Set parent of current weapon to the game manager instead, making room for new current weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindObjectOfType<GameManagerScript>().transform;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindObjectOfType<GameManagerScript>().transform.position;
                    //Set current weapon to selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon = GameManagerScript.instance.Launcher;
                    //Set parent of weapon to main camera
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.parent = GameObject.FindGameObjectWithTag("WeaponCamera").transform;
                    //Set local position and rotation of selected weapon
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelPosOffset;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.transform.localRotation = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>().currentWeapon.modelRotOffset;
                    GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.renderer.enabled = true;
                }
                GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.crosshair = GameObject.FindGameObjectWithTag("Crosshair");
            }
        }

        //"Use" key
        if (Input.GetKeyDown(KeyCode.E))
        {
            //Check if player has a camera component
            //if (this.GetComponentInChildren<Camera>() && this.GetComponentInChildren<Camera>().gameObject.tag != "WeaponCamera")
            //if (this.GetComponent<Camera>())
            cameraObject = GameObject.FindGameObjectWithTag("MainCamera");
            {
                //Create a raycast from the center of the camera
                //Vector is normalized so 0.5F is 50% of the screen size
                Ray usingRay = cameraObject.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
                //Ray usingRay = camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
                RaycastHit hit;

                //Check if the ray hits, 10 is the range
                if (Physics.Raycast(usingRay, out hit, 10))
                    //If it hits, check if the object hit has a shopscript component
                    if (hit.collider.GetComponent<ShopScript>())
                    {
                        //If it does, call the OpenShop function from the shopscript
                        hit.collider.gameObject.GetComponent<ShopScript>().OpenShop();

                    }
            }
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            GameManagerScript.instance.UsePowerupFromList();
        }
    }
}
