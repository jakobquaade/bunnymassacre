﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour
{
	public Vector3 minMagnitude; //Minimum offset the object will be moved each shake
	public Vector3 maxMagnitude; //Maximum offset the object will be moved each shake
	public Vector3 minRotation; //Minimum rotation the object will be rotated each shake
	public Vector3 maxRotation; //Maximum rotation the object will be rotated each shake
	
	public bool isSmoothed; //if true, the max magnitude and rotation is linearly reduced to the minimun magnitude and rotation, over the duration.

	public float duration; //how long the shake should be running, in seconds.
	public float curDuration;

	public Vector3 originPos;
	public Quaternion originRot;

	public Shake(Vector3 minMag, Vector3 maxMag, Vector3 minRot, Vector3 maxRot, Vector3 originPos, Quaternion originRot, bool isSmooth, float duration)
	{
		minMagnitude = minMag;
		maxMagnitude = maxMag;
		minRotation = minRot;
		maxRotation = maxRot;
		isSmoothed = isSmooth;
		this.duration = duration;
		curDuration = duration;
		this.originPos = originPos;
		this.originRot = originRot;
	}
}
