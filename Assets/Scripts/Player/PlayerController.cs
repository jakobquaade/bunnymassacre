﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float speed;

    public int pointsOnPickUp = 5;
    public int pointsOnPowerUp = 5;

    

    void Start()
    {
        Config.CurrentScore = 0;
        GameManagerScript.instance.Currency = 0;
        GameManagerScript.instance.StartGame();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Currency")
        {
            GameManagerScript.instance.Currency++;
            Destroy(other.gameObject);
            //SetCountText();
        }
        //else if (other.gameObject.tag == "Carrot") 
        //{
        //    Carrot carrotScript = other.gameObject.GetComponent<Carrot>();
        //    carrotScript.PickUp();
        //}
        else if (other.gameObject.tag == "Powerup")
        {
            PowerupDrop powerupScript = other.gameObject.GetComponent<PowerupDrop>();
            powerupScript.AddPowerupToList();
            //powerupScript.UsePowerup();
            Config.CurrentScore += pointsOnPowerUp;
            Debug.Log("Points for powerup");
            //SetCountText();
        }
        //else if (other.gameObject.tag == "PickUp")
        //{
        //    GameManagerScript.instance.ChangeToNight();
        //    other.gameObject.SetActive(false);
        //    Debug.Log("PickUp");
        //}
    }
}