﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

    public WeaponBase currentWeapon;

    public void Start()
    {
        
    }

    public void Update()
    {
        //If left mouse is held down
        if(Input.GetMouseButton(0))
        
        //If left mouse is pressed once
        //if (Input.GetMouseButtonDown(0))
        {
            //print("Mouse0 was pressed");
            Attack();
            
        }

        if(Input.GetMouseButton(1))
        {
            Attack2();
        }
        if (currentWeapon != null)
            currentWeapon.Update();
    }

    /// <summary>
    /// Attack with current weapon
    /// </summary>
    public void Attack()
    {
        //Check if player is holding a weapon
        if (currentWeapon != null)
        {
            //Check if weapon is a ranged type, then check if any ammunition is left and cooldown is over
            if (currentWeapon.weaponType == WeaponType.Ranged && currentWeapon.ammunition > 0 && Time.time - currentWeapon.lastAttack > currentWeapon.coolDown)
            {
                currentWeapon.Attack();
                currentWeapon.lastAttack = Time.time;
            }
            //If weapon is a melee type, check if cooldown is over
            else if (currentWeapon.weaponType == WeaponType.Melee && Time.time - currentWeapon.lastAttack > currentWeapon.coolDown)
            {
                currentWeapon.Attack();
                currentWeapon.lastAttack = Time.time;
            }
            //If cooldown is active or ammo is depleted
            else
            {
                //Check if melee
                if (currentWeapon.weaponType == WeaponType.Melee)
                {
                    print("Attack is on cooldown!");
                }
                //Check if ammo is depleted
                else
                {
                    if (currentWeapon.ammunition > 0)
                    {
                        print("Ranged attack is on cooldown!");
                    }
                    else
                    {
                        print("Out of ammo!");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Attack with current weapon
    /// </summary>
    public void Attack2()
    {
        //Check if player is holding a weapon
        if (currentWeapon != null)
        {
            //Check if weapon is a ranged type, then check if any ammunition is left and cooldown is over
            if (currentWeapon.weaponType == WeaponType.Ranged && currentWeapon.ammunition2 > 0 && Time.time - currentWeapon.lastAttack2 > currentWeapon.coolDown2)
            {
                currentWeapon.Attack2();
                currentWeapon.lastAttack2 = Time.time;
            }
            //If weapon is a melee type, check if cooldown is over
            else if (currentWeapon.weaponType == WeaponType.Melee && Time.time - currentWeapon.lastAttack2 > currentWeapon.coolDown2)
            {
                currentWeapon.Attack2();
                currentWeapon.lastAttack2 = Time.time;
            }
            //If cooldown is active or ammo is depleted
            else
            {
                //Check if melee
                if (currentWeapon.weaponType == WeaponType.Melee)
                {
                    print("Attack is on cooldown!");
                }
                //Check if ammo is depleted
                else
                {
                    if (currentWeapon.ammunition2 > 0)
                    {
                        print("Ranged attack is on cooldown!");
                    }
                    else
                    {
                        print("Out of ammo!");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Functionality for switching weapons
    /// Not used yet
    /// </summary>
    /// <param name="weaponName"></param>
    public void SwitchWeapon(WeaponBase weapon)
    {
        if (weapon != null)
        {
            if (weapon.GetComponent<LauncherScript>())
            {
                weapon.renderer.enabled = false;
            }
            else
            {
                MeshRenderer[] tempList;
                tempList = GameManagerScript.instance.player.GetComponent<Weapon>().currentWeapon.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer x in tempList)
                {
                    x.renderer.enabled = false;
                }
            }
                
        }

        
        GameManagerScript.instance.player.GetComponent<PlayerSound>().audio.PlayOneShot(GameManagerScript.instance.player.GetComponent<PlayerSound>().playerSounds[4]);
    }
}
