﻿using UnityEngine;
using System.Collections;

public enum State { Standing, ForwardRun, BackwardRun, LeftRun, RightRun, Jumping };

public class CameraBob : MonoBehaviour
{

    public State state;

    //
    private Vector3 originPos; //where the camera should be, when standing still.
    private Quaternion originRot;

    public float toStandingSpeed; //How fast should the camera return to standing position when stopping.

    public float runForwardHeightBob; //How high the camera will be at its peak when running forward
    public float runForwardBobSpeed; //How fast to bob

    public float runBackwardHeightBob; //How high the camera will be at its peak when running forward
    public float runBackwardBobSpeed; //How fast to bob

    public float runLeftHeightBob; //How high the camera will be at its peak when running forward
    public float runLeftBobSpeed; //How fast to bob
    public Vector3 runLeftRotation; //How much to tilt the camera

    public float runRightHeightBob; //How high the camera will be at its peak when running forward
    public float runRightBobSpeed; //How fast to bob
    public Vector3 runRightRotation; //How much to tilt the camera

    private float bobUp = 0;

    void Awake()
    {
        originPos = transform.localPosition;
        originRot = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {

        //When standing, simply tween the camera to it's origin position and rotation.
        if (state == State.Standing)
        {
            transform.localRotation = Quaternion.Slerp(transform.localRotation, originRot, toStandingSpeed * Time.deltaTime);
            transform.localPosition = Vector3.Lerp(transform.localPosition, originPos, toStandingSpeed * Time.deltaTime);

        }
        //Move camera up/down as well as to the sides.
        if (state == State.ForwardRun)
        {
            bobUp = Mathf.PingPong(runForwardBobSpeed * Time.time, runForwardHeightBob);

            Vector3 newPos = new Vector3(transform.localPosition.x, bobUp, transform.localPosition.z);
            transform.localPosition = Vector3.Lerp(transform.localPosition, newPos, toStandingSpeed * Time.deltaTime);

            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.identity, toStandingSpeed * Time.deltaTime);

        }
        if (state == State.BackwardRun)
        {
            //For now, we just reuse the forward running bobbing.
            bobUp = Mathf.PingPong(runBackwardBobSpeed * Time.time, runBackwardHeightBob);
            transform.localPosition = new Vector3(transform.localPosition.x, bobUp, transform.localPosition.z);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.identity, toStandingSpeed * Time.deltaTime);
        }
        if (state == State.LeftRun)
        {
            //slight up/down bobbing
            bobUp = Mathf.PingPong(runLeftBobSpeed * Time.time, runLeftHeightBob);
            transform.localPosition = new Vector3(transform.localPosition.x, bobUp, transform.localPosition.z);

            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(runLeftRotation), toStandingSpeed * Time.deltaTime);

        }
        if (state == State.RightRun)
        {
            //slight up/down bobbing
            bobUp = Mathf.PingPong(runRightBobSpeed * Time.time, runRightHeightBob);
            transform.localPosition = new Vector3(transform.localPosition.x, bobUp, transform.localPosition.z);

            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(runRightRotation), toStandingSpeed * Time.deltaTime);
        }
        if (state == State.Jumping)
        {
            transform.localRotation = Quaternion.Slerp(transform.localRotation, originRot, toStandingSpeed * Time.deltaTime);
            transform.localPosition = Vector2.Lerp(transform.localPosition, originPos, toStandingSpeed * Time.deltaTime);

        }

    }
}
