﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;

public static class Config
{
    private static int currentScore;

    public static int CurrentScore
    {
        get { return currentScore; }
        set { currentScore = value; }
    }
    

    private static int[] highscores;

    public static int[] Highscores
    {
        get { return highscores; }
        set { highscores = value; }
    }

    private static float masterVolume;

    public static float MasterVolume
    {
        get { return masterVolume; }
        set { masterVolume = value; }
    }

    private static float musicVolume;

    public static float MusicVolume
    {
        get { return musicVolume; }
        set { musicVolume = value; }
    }

    private static float soundFXVolume;

    public static float SoundFXVolume
    {
        get { return soundFXVolume; }
        set { soundFXVolume = value; }
    }

    private static string filepath = Application.dataPath + "/Resources/config.xml";
    private static bool isNew;
    
    



    static Config()
    {
        highscores = new int[11];
        if (!File.Exists(filepath)) //Makes a new file, if it doesn't exist
        {
            masterVolume = 100;
            musicVolume = 100;
            soundFXVolume = 100;

            

            UpdateConfig();
            Debug.Log("Made new Config file");
        }
        else 
        {
            //Reads from config-file and updates variables
            XmlDocument doc = new XmlDocument();
            doc.Load(filepath);
            if (doc.DocumentElement == null)
            {
                masterVolume = 100;
                musicVolume = 100;
                soundFXVolume = 100;


                UpdateConfig();
            }
            ReadConfig();
            Debug.Log("Read from Config File");
        }
    }

    /// <summary>
    /// Compares score to highscores and sorts the list
    /// </summary>
    public static void CheckScore()
    {
        isNew = true;
        for (int i = 0; i < highscores.Length; i++)
        {
            if (highscores[i] == currentScore)
            {
                isNew = false;
            }
        }
        if (isNew)
        {
            highscores[10] = currentScore;
            System.Array.Sort(highscores);
            System.Array.Reverse(highscores);
            UpdateConfig();
            Debug.Log("New score added");
        }
        
    }
    

    /// <summary>
    /// Method that builds Config.xml based on the highscores array
    /// </summary>
    public static void UpdateConfig()
    {
        XmlDocument doc = new XmlDocument();
        XmlNode node = doc.CreateElement("Config");
        XmlNode subNode;
        doc.AppendChild(node);
        node = doc.CreateElement("Highscores");
        subNode = doc.CreateElement("FirstPlace");
        subNode.InnerText = highscores[0].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("SecondPlace");
        subNode.InnerText = highscores[1].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("ThirdPlace");
        subNode.InnerText = highscores[2].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("FourthPlace");
        subNode.InnerText = highscores[3].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("FifthPlace");
        subNode.InnerText = highscores[4].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("SixthPlace");
        subNode.InnerText = highscores[5].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("SeventhPlace");
        subNode.InnerText = highscores[6].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("EigthPlace");
        subNode.InnerText = highscores[7].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("NinthPlace");
        subNode.InnerText = highscores[8].ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("TenthPlace");
        subNode.InnerText = highscores[9].ToString();
        node.AppendChild(subNode);
        doc.DocumentElement.AppendChild(node);
        node = doc.CreateElement("Volumes");
        subNode = doc.CreateElement("Master");
        subNode.InnerText = masterVolume.ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("Music");
        subNode.InnerText = musicVolume.ToString();
        node.AppendChild(subNode);
        subNode = doc.CreateElement("SoundFX");
        subNode.InnerText = soundFXVolume.ToString();
        node.AppendChild(subNode);
        doc.DocumentElement.AppendChild(node);
        doc.Save(filepath);
    }

    /// <summary>
    /// Method that reads Config data from config.xml
    /// </summary>
    static void ReadConfig()
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(filepath);
        foreach (XmlElement element in doc.DocumentElement)
        {
            switch (element.Name)
            {
                case "Highscores":
                    int score;
                    foreach (XmlElement e in element)
                    {
                        switch (e.Name)
                        {
                            case "FirstPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[0] = score;
                                break;

                            case "SecondPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[1] = score;
                                break;

                            case "ThirdPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[2] = score;
                                break;
                            case "FourthPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[3] = score;
                                break;
                            case "FifthPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[4] = score;
                                break;
                            case "SixthPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[5] = score;
                                break;
                            case "SeventhPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[6] = score;
                                break;
                            case "EighthPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[7] = score;
                                break;
                            case "NinthPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[8] = score;
                                break;
                            case "TenthPlace":
                                int.TryParse(e.InnerText, out score);
                                highscores[9] = score;
                                break;
                        }

                    }
                    break;

                case "Volumes":
                    
                    float volume;
                    foreach (XmlElement e in element)
                    {
                        switch (e.Name)
                        {
                            case "Master":
                                float.TryParse(e.InnerText, out volume);
                                masterVolume = volume;
                                break;

                            case "Music":
                                float.TryParse(e.InnerText, out volume);
                                musicVolume = volume;
                                break;

                            case "SoundFX":
                                float.TryParse(e.InnerText, out volume);
                                soundFXVolume = volume;
                                break;
                        }

                    }
                    break;
            }
        }
    }

}
